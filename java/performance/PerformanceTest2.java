package performance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PerformanceTest2 {

	private static final int COMPONENT_SIZE = 100000;
	private static Random random = new Random();

	public static void executionTime(String testName, long startTime) {

		long stopTime = 0;
		long executionTime = 0;
		// get the time of the start of the test
		stopTime = System.nanoTime();

		// get the time of the execution for the test
		executionTime = stopTime - startTime;

		System.out.println("\nTest Name:      " + testName);
		System.out.println("Start Time:     " + startTime);
		System.out.println("Stop Time:      " + stopTime);
		System.out.println("Execution Time: " + executionTime);
	}

	public static void loopTime() {

		// get the time of the start of the test
		long startTime = System.nanoTime();

		for (int i = 0; i < 100; i++) {
			System.out.print(" " + i);
		}

		executionTime("loopTime", startTime);
	}

	public static void staticArray() {

		// get the time of the start of the test
		long startTime = System.nanoTime();

		// create static array
		int[] staticArray = new int[COMPONENT_SIZE];

		// load static array
		for (int i = 0; i < COMPONENT_SIZE; i++) {
			staticArray[i] = random.nextInt(COMPONENT_SIZE);
		}

		executionTime("staticArray", startTime);
	}

	public static void dynamicArray() {

		// get the time of the start of the test
		long startTime = System.nanoTime();

		// create dynamic list
		List<Integer> dynamicList = new ArrayList<Integer>();

		// load dynamic list
		for (int i = 0; i < COMPONENT_SIZE; i++) {
			dynamicList.add(random.nextInt(COMPONENT_SIZE));
		}

		executionTime("dynamicList", startTime);
	}

	public static void main(String[] args) {
		PerformanceTest2.loopTime();
		PerformanceTest2.staticArray();
		PerformanceTest2.dynamicArray();
	}
}
