package performance;

/**
 * println is faster, outputs a String. printf is slower, required reformatting
 * the String.
 */
public class PerformanceTest1 {

	public static void main(String[] args) {

		long startPrintln = 0;
		long stopPrintln = 0;
		long startPrintf = 0;
		long stopPrintf = 0;

		startPrintln = System.nanoTime();
		for (int i = 0; i < 100; i++) {
			System.out.println("The number is " + i);
		}

		stopPrintln = System.nanoTime() - startPrintln;

		startPrintf = System.nanoTime();
		for (int i = 0; i < 100; i++) {
			System.out.printf("The number is " + i);
		}

		stopPrintf = System.nanoTime() - startPrintf;

		System.out.println("\n");
		System.out.println("println took: " + stopPrintln + " nano seconds");
		System.out.printf("printf took: " + stopPrintf + " nano seconds");
	}
}