package graphics;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class ImageProcessorTest {

	@Test
	public void readWriteImages() throws IOException {

		Image image = new Image("files/imageIn1.jpg", "jpg");
		ImageProcessor imageProcessor = new ImageProcessor(image);
		int[][] pixels = imageProcessor.image2pixels();
		ImageProcessor.pixels2image("files/imageOut.jpg", "jpg", pixels);
	}

	@Test
	public void differences() throws IOException {

		Image image1 = new Image("files/imageIn1.jpg", "jpg");
		Image image2 = new Image("files/imageIn2.jpg", "jpg");

		ImageProcessor imageProcessor1 = new ImageProcessor(image1);
		ImageProcessor imageProcessor2 = new ImageProcessor(image2);

		int[][] pixelsImage3 = ImageProcessor.difference(imageProcessor1, imageProcessor2);
		ImageProcessor.pixels2image("files/difference.jpg", "jpg", pixelsImage3);
	}
}
