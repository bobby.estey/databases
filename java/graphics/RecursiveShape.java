// https://www.mkyong.com/awt/java-awt-drawing-rectangle-line-and-circle/
package graphics;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class RecursiveShape extends JFrame {

	private static final long serialVersionUID = 1L;
	private int x = 100;
	private int y = 100;
	private int width = 100;
	private int height = 100;

	public RecursiveShape() {

		// set's the size of the frame to 320 x 320
		setSize(new Dimension(320, 320));

		// when the close button is pressed, exit the application
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// make the frame visible
		setVisible(true);

		// sets the title of the frame
		setTitle("Recursive Shape");

		// component inside the frame
		JPanel panel = new JPanel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override

			// a component that can be rendered
			public void paintComponent(Graphics graphics) {
				Graphics2D graphics2D = (Graphics2D) graphics;
				drawShape(graphics2D);
			}
		};

		// we add the panel to the frame
		this.getContentPane().add(panel);
	}

	// draws a shape
	public void drawShape(Graphics2D graphics2D) {
		Ellipse2D ellipse2d = new Ellipse2D.Double(x, y, width, height);
		graphics2D.draw(ellipse2d);

		if (x >= 0) {
			x -= 1;
			// y -= 1;
			width += 1;
			height += 1;
			drawShape(graphics2D);
		}
	}

	public void drawText(Graphics2D graphics2D, String string, int x, int y) {
		graphics2D.drawString(string, x, y); // print on the screen at coordinates x / y the string
	}
}