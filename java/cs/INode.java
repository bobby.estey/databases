/**
 * INode - simple interface for a Node in a linked list
 * @author cv64
 */
package cs;

public interface INode {

	public int getData();

	public void setData(int data);
}
