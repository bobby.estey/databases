// https://stackoverflow.com/questions/14353302/displaying-image-in-java
package swing;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class DisplayImage extends JFrame {

	private static final long serialVersionUID = 4005531722057462207L;

	// windows path example - c:\\users\\rwe001\\logo.gif
	String imageFile = "files/logo.gif";

	public DisplayImage() throws IOException {

		BufferedImage bufferedImage = ImageIO.read(new File(imageFile));
		ImageIcon imageIcon = new ImageIcon(bufferedImage);

		setLayout(new FlowLayout());
		setSize(150, 150);
		JLabel label = new JLabel();
		label.setIcon(imageIcon);
		add(label);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String avg[]) throws IOException {
		new DisplayImage();
	}
}
