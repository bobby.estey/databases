package swing;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class JTabbedPaneExample extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final ImageIcon icon = new ImageIcon("logo.png");

	public JTabbedPaneExample() {

		// super - calling the SUPER CLASS Constructor
		// in this case the Super Class Constructor that accepts a String
		// and set the Frame Title
		super("JTabbedPane Example");

		// once the user selects the exit button, close the Frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// sets the size of the Frame
		setSize(300, 100);

		// make the Frame visible
		setVisible(true);

		// Container that is wrapping all of the widgets (One, Two)
		JTabbedPane jTabbedPane = new JTabbedPane();
		JPanel jpanel1 = createPanel("Tab 1 Panel");
		jTabbedPane.addTab("Tab One", icon, jpanel1, "Tab 1");
		JPanel jpanel2 = createPanel("Tab 2 Panel");
		jTabbedPane.addTab("Tab Two", icon, jpanel2, "Tab 2");
		add(jTabbedPane); // add to the JFrame
	}

	protected JPanel createPanel(String text) {
		JPanel panel = new JPanel();
		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(JLabel.CENTER);
		panel.setLayout(new GridLayout(1, 1));
		panel.add(label);
		return panel;
	}

	public static void main(String args[]) {
		new JTabbedPaneExample();
	}
}