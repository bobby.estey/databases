// https://stackoverflow.com/questions/14353302/displaying-image-in-java
package swing;

import java.awt.FlowLayout;
import java.awt.image.DataBufferByte;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class DisplayImages extends JFrame {

	private static final long serialVersionUID = 4005531722057462207L;

	// windows path example - c:\\users\\rwe001\\logo.gif
	String imageFile = "files/picture1.jpg";
	
    public int width;
    public int height;
    private boolean hasAlphaChannel;
    private int pixelLength;
    private byte[] pixels;

    public void fastRGB(BufferedImage image) {
        pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        width = image.getWidth();
        height = image.getHeight();
        hasAlphaChannel = image.getAlphaRaster() != null;
        pixelLength = 3;
        if (hasAlphaChannel)
            pixelLength = 4;
    }

    public short[] getRGB(int x, int y) {
        int pos = (y * pixelLength * width) + (x * pixelLength);
        short rgb[] = new short[4];
        if (hasAlphaChannel)
            rgb[3] = (short) (pixels[pos++] & 0xFF); // Alpha
        rgb[2] = (short) (pixels[pos++] & 0xFF); // Blue
        rgb[1] = (short) (pixels[pos++] & 0xFF); // Green
        rgb[0] = (short) (pixels[pos++] & 0xFF); // Red
        return rgb;
    }

	@SuppressWarnings("unused")
	public DisplayImages() throws IOException {

		BufferedImage bufferedImage = ImageIO.read(new File(imageFile));
		ImageIcon imageIcon = new ImageIcon(bufferedImage);
		
		fastRGB(bufferedImage);

//		int width = bufferedImage.getWidth();
//		int height = bufferedImage.getHeight();
//
//		double[][] data = new double[width][height];
//
//		for (int i = 0; i < width * height; i++) {
//			bufferedImage.getData().getPixels(0, 0, width, height, data[i]);
//		}

		setLayout(new FlowLayout());
		setSize(150, 150);
		JLabel label = new JLabel();
		label.setIcon(imageIcon);
		add(label);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		BufferedImage bufImgs = ImageIO.read(new File("c:\\adi.bmp"));
		// double[][] data = new double[][];
		// bufImgs.getData().getPixels(0,0,bufImgs.getWidth(),bufImgs.getHeight(),data[i]);
	}

	public static void main(String avg[]) throws IOException {
		new DisplayImages();
	}
}
