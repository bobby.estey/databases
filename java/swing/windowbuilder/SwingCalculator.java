// https://www.youtube.com/watch?v=VPNs0OA4isk
package swing.windowbuilder;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SwingCalculator extends JFrame {

	private static final long serialVersionUID = -8223780258397043L;
	private JPanel contentPane;
	private JTextField textField1;
	private JTextField textField2;
	private JButton btnAdd;
	private JLabel lblTotal;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SwingCalculator frame = new SwingCalculator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public SwingCalculator() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		textField1 = new JTextField();
		textField1.setFont(new Font("Dialog", Font.BOLD, 20));
		textField1.setBackground(Color.YELLOW);
		textField1.setText("Number1");

		GridBagConstraints gbc_textField1 = new GridBagConstraints();
		gbc_textField1.insets = new Insets(0, 0, 5, 0);
		gbc_textField1.anchor = GridBagConstraints.WEST;
		gbc_textField1.gridx = 2;
		gbc_textField1.gridy = 1;

		contentPane.add(textField1, gbc_textField1);
		textField1.setColumns(10);
		textField2 = new JTextField();
		textField2.setBackground(Color.GREEN);
		textField2.setFont(new Font("Dialog", Font.BOLD, 20));
		textField2.setText("Number2");

		GridBagConstraints gbc_textField2 = new GridBagConstraints();
		gbc_textField2.insets = new Insets(0, 0, 5, 0);
		gbc_textField2.anchor = GridBagConstraints.WEST;
		gbc_textField2.gridx = 2;
		gbc_textField2.gridy = 2;

		contentPane.add(textField2, gbc_textField2);
		textField2.setColumns(10);

		btnAdd = new JButton("ADD");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int one = Integer.parseInt(textField1.getText());
				int two = Integer.parseInt(textField2.getText());

				int answer = one + two;

				lblTotal.setText(Integer.toString(answer));
			}
		});

		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdd.gridx = 2;
		gbc_btnAdd.gridy = 3;
		contentPane.add(btnAdd, gbc_btnAdd);

		lblTotal = new JLabel("Total");
		lblTotal.setForeground(Color.BLUE);
		lblTotal.setBackground(Color.YELLOW);
		GridBagConstraints gbc_lblTotal = new GridBagConstraints();
		gbc_lblTotal.gridx = 2;
		gbc_lblTotal.gridy = 5;
		contentPane.add(lblTotal, gbc_lblTotal);
	}
}
