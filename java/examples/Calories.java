
package examples;

/* Write a program using inputs age (years), weight (pounds), heart rate (beats per minute) and time (minutes), respectively.  
 * Output calories burned for women and men.  Output each floating point value with two digits after the decimal point.
*/
import java.util.Scanner;

public class Calories {

	// keyboard - object that is accepting the keyboard entries
	// System.in - is the keyboard itself
	private static Scanner keyboard = new Scanner(System.in);

	private static int age = 0;
	private static int weight = 0;
	private static int heartRate = 0;
	private static int time = 0;
	private static double womenCalories = 0;
	private static double menCalories = 0;

	// do work, methods are functions f(x) - should only do one thing
	// method getInput - gets the users input, age, weight, heartRate and time
	public static void getInput() {

		System.out.println("Enter the Age Weight HeartRate TimeMinutes, e.g. 49 155 148 60");
		age = keyboard.nextInt();
		weight = keyboard.nextInt();
		heartRate = keyboard.nextInt();
		time = keyboard.nextInt();
	}

	// method getOutput - computes the calories
	public static void getOutput() {

		womenCalories = ((age * 0.074) - (weight * 0.05741) + (heartRate * 0.4472) - 20.4022) * time / 4.184;
		menCalories = ((age * 0.2017) + (weight * 0.09036) + (heartRate * 0.6309) - 55.0969) * time / 4.184;

		System.out.print("Women: ");
		System.out.printf("%.2f", womenCalories);
		System.out.println(" calories");

		System.out.print("Men: ");
		System.out.printf("%.2f", menCalories);
		System.out.println(" calories");
	}

	// main method - instructs the Java Virtual Machine (JVM) where to start
	// main method - should have the least amount of implementation
	public static void main(String[] args) {

		Calories.getInput();
		Calories.getOutput();
	}
}
