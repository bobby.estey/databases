package student;

import java.io.Serializable;

class Student implements Serializable {

	private static final long serialVersionUID = 1L;
	private int SID;
	private int scores[] = new int[5];

	public int getSID() {
		return SID;
	}

	public void setSID(int sID) {
		SID = sID;
	}

	public int[] getScores() {
		return scores;
	}

	public void setScores(int[] scores) {
		this.scores = scores;
	}

	// Serializable
	@Override
	public String toString() {

		String scoresString = "";
		for (int i = 0; i < scores.length; i++) {
			scoresString += scores[i] + "\t";
		}

		String record = SID + "\t" + scoresString + "\n";

		return record;
	}
}