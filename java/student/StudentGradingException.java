package student;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

//package exception;

public class StudentGradingException extends Exception {

	private static final long serialVersionUID = 1L;
	private BufferedWriter bufferedWriter = null;
	private FileWriter fileWriter = null;
	private String errorFilename = "error.log";

	public StudentGradingException(String message) throws Exception {

		System.err.println("StudentGradingException: " + message);
		throw new Exception(message);
	}

	public StudentGradingException() {
		openFile();
	}

	public int checkNumberOfStudents(String numberOfStudents) {

		int studentCount = 0;

		try {
			studentCount = Integer.parseInt(numberOfStudents);
		} catch (NumberFormatException e) {
			String errorMessage = "Invalid Number Format Exception: " + e.getMessage();
			write2File(errorMessage);
			studentCount = 40;
		} finally {
			// do something else
		}

		return studentCount;
	}

	public void write2File(String message) {
		try {
			System.err.println(message);
			bufferedWriter.write(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// create file to write to
	public void openFile() {

		try {
			fileWriter = new FileWriter(errorFilename);
			bufferedWriter = new BufferedWriter(fileWriter);

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}
	}

	public void closeFile() {

		try {
			if (bufferedWriter != null)
				bufferedWriter.close();

			if (fileWriter != null)
				fileWriter.close();
		} catch (IOException ex) {
			System.err.format("IOException: %s%n", ex);
		}
	}
}
