package student;

public class Driver4 {

	public static void main(String[] args) {

		Student lab2[] = new Student[40];
		String courses[] = { "chemistry", "computerScience" };

		// Populate the student array
		try {

			for (int i = 0; i < courses.length; i++) {
				
				Util.counter = -1;

				lab2 = Util.readFile(courses[i] + ".txt", lab2);

				int count = Util.counter;

				Statistics statlab2 = new Statistics();

				statlab2.printValues(lab2, count);
				statlab2.findlow(lab2, count);
				statlab2.findhigh(lab2, count);

				// read a second time, due to the readFile method being static
				lab2 = Util.readFile(courses[i] + ".txt", lab2);

				statlab2.findavg(lab2, count);

				String statisticsString = statlab2.getOutput();

				// write the values to an output file
				StudentGrade studentGrade = new StudentGrade(lab2, count, statisticsString);
				FileIO.write2File(courses[i] + ".out", studentGrade.toString());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}