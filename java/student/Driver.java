package student;

public class Driver {

	public static void main(String[] args) {

		Student lab2[] = new Student[40];

		// Populate the student array
		try {
			lab2 = Util.readFile("students.txt", lab2);

			int count = Util.counter;

			Statistics statlab2 = new Statistics();

			statlab2.printValues(lab2, count);
			statlab2.findlow(lab2, count);
			statlab2.findhigh(lab2, count);

			// read a second time, due to the readFile method being static
			lab2 = Util.readFile("students.txt", lab2);

			statlab2.findavg(lab2, count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}