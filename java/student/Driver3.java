package student;

public class Driver3 {

	public static void main(String[] args) {

		Student lab2[] = new Student[40];

		// Populate the student array
		try {
			lab2 = Util.readFile("students.txt", lab2);

			int count = Util.counter;

			StudentAPIImpl studentAPIImpl = new StudentAPIImpl(lab2, count);
			studentAPIImpl.printStudentStatistics();

			Student student0 = lab2[0];
			studentAPIImpl.printScore(student0);
			
			Student student4 = lab2[4];
			studentAPIImpl.printScore(student4);

			Statistics statlab2 = new Statistics();

			statlab2.printValues(lab2, count);
			statlab2.findlow(lab2, count);
			statlab2.findhigh(lab2, count);

			// read a second time, due to the readFile method being static
			lab2 = Util.readFile("students.txt", lab2);

			statlab2.findavg(lab2, count);

			String statisticsString = statlab2.getOutput();

			// write the values to an output file
			StudentGrade studentGrade = new StudentGrade(lab2, count, statisticsString);
			FileIO.write2File("lab2.out", studentGrade.toString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}