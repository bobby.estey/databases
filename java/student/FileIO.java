package student;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileIO {

	public static void write2File(String filename, String string) {

		try {
			FileWriter fileWriter = new FileWriter(filename);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			bufferedWriter.write(string);

			if (bufferedWriter != null)
				bufferedWriter.close();

			if (fileWriter != null)
				fileWriter.close();
		} catch (IOException ex) {
			System.err.format("IOException: %s%n", ex);
		}
	}
}
