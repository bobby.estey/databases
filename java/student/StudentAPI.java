package student;
// design - the agreement
public interface StudentAPI {

	public void printStudentStatistics();

	public void printScore(Student student);
}