package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * database example program
 */
public class JDBCExample {

	// if using mysql uncomment
	// my service provider does not allow Java to communicate directly
//	private static final String DATABASE = "jdbc:mysql://localhost:3306/mysql";
//	private static final String DATABASE = "jdbc:mysql://cv64us.fatcowmysql.com:3306/training";
//	private static final String USERNAME = "student";
//	private static final String PASSWORD = "pAs$w0rD";

	// if using mariadb uncomment
	private static final String DATABASE = "jdbc:mariadb://localhost:3306/cv64";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";

	// if using oracle uncomment - example:
	// jdbc:oracle:thin:@localhost:1521:xe
//	private static final String DATABASE = "jdbc:oracle:thin:@serverIp:port:database";  
//	private static final String USERNAME = "root";
//	private static final String PASSWORD = "root";

	private Connection connection = null;

	public JDBCExample() {
		System.out.println("JDBCExample()");
		getConnection();
		executeQueryExample();
		executeUpdateExample();
		closeConnection();
	}

	/**
	 * gets the database connection
	 */
	private void getConnection() {

		try {
			// Class.forName("com.mysql.cj.jdbc.Driver");
			Class.forName("org.mariadb.jdbc.Driver");
//			Class.forName("oracle.jdbc.driver.OracleDriver");

			System.out.println("JDBCExample.getConnection(): JDBC Driver Registered");

			connection = DriverManager.getConnection(DATABASE, USERNAME, PASSWORD);
//			connection = DriverManager
//					.getConnection("jdbc:mysql://cv64us.fatcowmysql.com/training?user=student&password=pAs$w0rD");
			System.out.println("JDBCExample.getConnection(): Connection Successful");

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCExample.getConnection(): JDBC Driver Missing");
			e.printStackTrace();

		} catch (SQLException e) {
			System.err.println("JDBCExample.getConnection(): Connection Failed");
			e.printStackTrace();
		}
	}

	/**
	 * READONLY - SELECT This examples performs the executeQuery method which is
	 * (select) statement Uses the connection to access the database, creates a
	 * database statement and gets the results of the statement.
	 */
	private void executeQueryExample() {
		Statement statement = null;
		ResultSet resultSet = null;
		String sql = "select * from cv64.example";

		// oracle example
//		String sql = "select dummy, sysdate, dummy from dual";

		try {
			statement = connection.createStatement();

			System.out.println("JDBCExample.executeQueryExample(): SQL: " + sql);
			resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String column1 = resultSet.getString(1);
				System.out.println("JDBCExample.executeQueryExample(): Column1: " + column1);

				String column2 = resultSet.getString(2);
				System.out.println("JDBCExample.executeQueryExample(): Column2: " + column2);

				String column3 = resultSet.getString(3);
				System.out.println("JDBCExample.executeQueryExample(): Column3: " + column3);
			}

		} catch (SQLException e) {
			System.err.println("JDBCExample.executeQueryExample(): Statement / ResultSet Failed");
			e.printStackTrace();

			// final clean up of resultSet and statement
		} finally {
			System.out.println("JDBCExample.executeQueryExample(): finally");
			if (resultSet != null) {
				resultSet = null;
				System.out.println("JDBCExample.executeQueryExample(): resultSet closed");
			}

			if (statement != null) {
				statement = null;
				System.out.println("JDBCExample.executeQueryExample(): statement closed");
			}
		}
	}

	/**
	 * WRITE - INSERT INTO, UPDATE, DELETE This examples performs the executeUpdate
	 * method which are (insert, update, delete) statements Uses the connection to
	 * access the database, creates a database statement and gets the results of the
	 * statement.
	 */
	private void executeUpdateExample() {
		Statement statement = null;
		String sql = "insert into example (first_name, last_name) values " + "('steve', 'smith')";
		int rows = 0;

		try {
			statement = connection.createStatement();

			System.out.println("JDBCExample.executeUpdateExample(): SQL: " + sql);
			rows = statement.executeUpdate(sql);

			System.out.println("JDBCExample.executeUpdateExample(): rows updated: " + rows);

		} catch (SQLException e) {
			System.err.println("JDBCExample.executeUpdateExample(): Statement / ResultSet Failed");
			e.printStackTrace();

			// final clean up of resultSet and statement
		} finally {
			System.out.println("JDBCExample.executeUpdateExample(): finally");

			if (statement != null) {
				statement = null;
				System.out.println("JDBCExample.executeUpdateExample(): statement closed");
			}
		}
	}

	/**
	 * closes the database connection
	 */
	private void closeConnection() {
		System.out.println("JDBCExample.closeConnection()");
		if (connection != null) {
			connection = null;
			System.out.println("JDBCExample.closeConnection(): connection closed");
		}
	}

	/**
	 * start the JDBC Example program and calls the no argument Constructor
	 * 
	 * @param argc
	 */
	public static void main(String[] argc) {

		new JDBCExample();
	}
}