package dateTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarUtility {

	public static final String TIME_ZERO = "1970-JAN-01 00:00:00";
	public static final long HOUR = 3600000L;
	public static final int[] MONTHS = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	public static final String[] dayNames = { "", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
			"Saturday" };

	/**
	 * keep from instantiating
	 * 
	 * CalendarUtility.java
	 */
	private CalendarUtility() {
	}

	/**
	 * returns a string that was formatted.
	 * 
	 * @param calendar
	 * @param dateFormat
	 * @return
	 */
	public static String calendar2String(Calendar calendar, String dateFormat) {
		String string = null;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

		if (calendar != null) {
			string = simpleDateFormat.format(calendar.getTime());
		}

		return string;
	}

	/**
	 * method date2milliseconds sets a date to milliseconds
	 * 
	 * @param year
	 * @param month
	 * 
	 *                package mil.tbmcsul.common;
	 * @param day
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static long date2milliseconds(int year, int month, int day, int hours, int minutes, int seconds) {
		Date date = new Date(year, month, day, hours, minutes, seconds);

		return date.getTime();
	}

	/**
	 * method date2String converts a date to string dateFormat - the date format to
	 * render, e.g. new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
	 */
	@SuppressWarnings("deprecation")
	public static String date2String(String dateFormat, int year, int month, int day, int hours, int minutes,
			int seconds) {
		Date date = new Date(year, month, day, hours, minutes, seconds);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

		return simpleDateFormat.format(date);
	}

	/**
	 * method getMilliseconds gets the milliseconds
	 * 
	 * @return get the milliseconds
	 */
	public static long getMilliseconds() {
		Calendar calendar = new GregorianCalendar();
		long milliseconds = calendar.getTimeInMillis();

		return milliseconds;
	}

	/**
	 * method getMSZulu2Local returns the current milliseconds of zulu to local time
	 * 
	 * @param currentMilliseconds
	 * @param hours
	 * @return
	 */
	public static long getMSZulu2Local(long currentMilliseconds, int hours) {
		return currentMilliseconds + (hours * (1000 * 60 * 60));
	}

	/**
	 * method milliseconds2String converts milliseconds to string
	 * 
	 * dateFormat - the date format to render, e.g.
	 * 
	 * new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
	 */
	public static String milliseconds2String(String timeZone, String dateFormat, long milliseconds) {
		if ((timeZone == null) || (timeZone.equals("")) || (timeZone.equals("LOCAL"))) {
			Calendar now = Calendar.getInstance();
			timeZone = now.getTimeZone().getID();
		}

		Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		calendar.setTimeInMillis(milliseconds);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * method setMilliseconds sets the milliseconds and get a date
	 * 
	 * @param milliseconds
	 * @return gets the date
	 */
	public static Date setMilliseconds(long milliseconds) {
		Calendar calendar = new GregorianCalendar();

		calendar.setTimeInMillis(milliseconds);

		return calendar.getTime();
	}

	/**
	 * method dateTime2ms - receives a date and time and returns the milliseconds
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 * @return milliseconds
	 */
	public static long dateTime2ms(int year, int month, int day, int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day, hour, minute, second);

		return calendar.getTimeInMillis();
	}

	/**
	 * returns the difference in long between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long difference(String startDate, String endDate) {

		Calendar start = string2Calendar(startDate);
		Calendar end = string2Calendar(endDate);

		return end.getTimeInMillis() - start.getTimeInMillis();
	}

	/**
	 * returns a calendar from milliseconds
	 * 
	 * @param milliseconds
	 * @return
	 */
	public static Calendar ms2Calendar(long milliseconds) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliseconds);

		return calendar;
	}

	/**
	 * Convert a String to Calendar - this method also checks if a 2 digit year was
	 * entered and converts to 4 digit year IAW the ICD.
	 * 
	 * @param String date string in format of yyyy-MMM-dd HH:mm:ss, e.g. 1970-JAN-01
	 *               00:00:00
	 * @return Calendar formatted object
	 */
	public static Calendar string2Calendar(String calendarString) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		Calendar calendar = null;
		Date date = null;

		try {
			date = simpleDateFormat.parse(calendarString);
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}

		catch (Exception exception) {
			System.err.println("string2Calendar: " + exception.getMessage());
		}

		return calendar;
	}

	/**
	 * Returns true if year is a leap year and false otherwise. Every 4 years is a
	 * leap year and if the century year is divisible by 400 1700, 1800, 1900 are
	 * not leap years
	 */
	private static boolean isLeapYear(int year) {

		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the number of leap years between year1 and year2, inclusive.
	 * Precondition: 0 <= year1 <= year2
	 */
	public static int numberOfLeapYears(int year1, int year2) {

		int totalLeapYears = 0;
		for (int year = year1; year <= year2; year++) {
			if (isLeapYear(year)) {
				totalLeapYears++;
			}
		}

		return totalLeapYears;
	}

	/**
	 * Returns the value representing the day of the week for the first day of year,
	 * where 0 denotes Sunday, 1 denotes Monday, ..., and 6 denotes Saturday.
	 */
	public static int firstDayOfYear(int year) {

		return dayOfWeek(1, 1, year);
	}

	/**
	 * Returns n, where month, day, and year specify the nth day of the year.
	 * Returns 1 for January 1 (month = 1, day = 1) of any year. Precondition: The
	 * date represented by month, day, year is a valid date.
	 * 
	 * http://mathforum.org/library/drmath/view/55837.html
	 */
	public static int dayOfYear(int year, int month, int day) {

		int doy = day;

		for (int i = 0; i < month; i++) {
			doy += MONTHS[i];
		}

		if (isLeapYear(year)) {
			if (month > 2) {
				doy += 1;
			} else if ((month == 2) && (day == 29)) {
				day += 1;
			}
		}

		return doy;
	}

	/**
	 * Returns the value representing the day of the week for the given date (month,
	 * day, year), where 0 denotes Sunday, 1 denotes Monday, ..., and 6 denotes
	 * Saturday. Precondition: The date represented by month, day, year is a valid
	 * date.
	 */
	public static int dayOfWeek(int year, int month, int day) {

		if (month == 1) {
			month = 13;
			year = year - 1;
		}
		if (month == 2) {
			month = 14;
			year = year - 1;
		}

		int mCalc = (3 * (month + 1)) / 5;
		int yCalc1 = year / 4;
		int yCalc2 = year / 100;
		int yCalc3 = year / 400;
		int N = day + (2 * month) + mCalc + year + yCalc1 - yCalc2 + yCalc3 + 2;

		int dow = (N % 7) - 1;

		if (dow == -1)
			return 6;
		return dow;
	}

	/**
	 * @param calendar
	 * @return
	 */
	public static String getDayOfWeek(Calendar calendar) {

		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return "Day of week is " + dayNames[dayOfWeek];
	}

	/**
	 * @param locale
	 * @param dateTime
	 */
	public static void getDateTime(Locale locale, String dateTime) {

		SimpleDateFormat localeDateFormat = null;
		Date longTime = null;

		try {
			localeDateFormat = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT,
					locale);

			longTime = localeDateFormat.parse(dateTime);

			System.out.println("dateTime:                                  " + dateTime);
			System.out.println("longTime:                                  " + longTime.getTime());
			System.out.println("localeDateFormatString.toPattern:          " + localeDateFormat.toPattern());
			System.out.println("localeDateFormatString.toLocalizedPattern: " + localeDateFormat.toLocalizedPattern());

		} catch (ParseException exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * @param simpleDateFormat
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String date2String(SimpleDateFormat simpleDateFormat, Date date) throws ParseException {

		String dateString = simpleDateFormat.format(date);
		return dateString;
	}

	public static void main(String[] args) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		try {
			System.out.println(date2String(simpleDateFormat, date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}