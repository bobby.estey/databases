package designPatterns.telescoping.separateBuilder;

public class PersonBuilder {
	private String firstName;
	private String lastName;

	public PersonBuilder() {
	}

	public PersonBuilder firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public PersonBuilder lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Person build() {
		return new Person(firstName, lastName);
	}
}