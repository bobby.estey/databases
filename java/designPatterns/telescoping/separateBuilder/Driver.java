package designPatterns.telescoping.separateBuilder;

public class Driver {
	public static void main(String[] args) {
		PersonBuilder personBuilder = new PersonBuilder();
		Person bobBuilder = personBuilder.firstName("Bob").lastName("Builder").build();
		
		System.out.println(bobBuilder.toString());
	}
}