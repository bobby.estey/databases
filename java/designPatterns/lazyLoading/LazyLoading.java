package designPatterns.lazyLoading;

public class LazyLoading {

	public static void main(String[] args) {

		SimpleClass simpleClass = new SimpleClass("string");
		System.out.println("simpleClass.getInteger(): " + simpleClass.getInteger());
		System.out.println("simpleClass.getInteger(): " + simpleClass.getInteger());
		System.out.println("simpleClass.getInteger(): " + simpleClass.getInteger());

		System.out.println("End Program");
	}
}
