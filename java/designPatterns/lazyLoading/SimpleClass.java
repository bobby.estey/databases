// https://www.youtube.com/watch?v=kUqV_8KfdTM
// https://android.jlelse.eu/lazy-initialisation-whats-a-correct-implementation-64c4638561e
// https://www.geeksforgeeks.org/lazy-loading-design-pattern/
// lazy loading - only load objects when needed
package designPatterns.lazyLoading;

public class SimpleClass {

	String string = "";

	// volatile - guarantees the latest value across all threads is written to main
	// memory immediately
	private volatile Integer integer = null;

	public SimpleClass(String string) {
		System.out.println("SimpleObject Constructor(" + string + ")");
		this.string = string;
	}

	public Integer getInteger() {
		System.out.println("SimpleObject.getInteger()");
		if (integer == null) {
			synchronized (this) {
				System.out.println("SimpleObject.getInteger() initializing");
				integer = Integer.valueOf(1);
			}
		}

		return integer;
	}
}
