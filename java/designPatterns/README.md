# Design Patterns
 - adapter - a class that provides implementation for an interface's required methods.  Converts an object so that another object can utilize
     - Interface MouseListener (5 methods that require implementation).  The Class MouseAdapter has 5 implementations of which a Concrete Class implementing the Interface only needs to define 0 to 5 methods.
     - with adapter - MouseListener (5) → MouseAdapter (5) → Concrete Class (0, 1, 2, 3, 4, 5 implementations)
     - without adapter - MouseListener (5) → MyClass(5)

 - builder -
 - factory - 
 - lazy loading - only load objects when needed
 - observer - watches events to occur and reports to others of the changes
 - singleton - only one instance of the class
 - strategy - defining a program and a strategy to solve
 - telescoping -
 
# Links
 - [adapter](https://www.geeksforgeeks.org/adapter-pattern/)
 - [builder](https://howtodoinjava.com/design-patterns/creational/builder-pattern-in-java/)
 - [factory](https://www.javatpoint.com/factory-method-design-pattern)
 - lazy loading
     - [video](https://www.youtube.com/watch?v=kUqV_8KfdTM)
     - [website 1](https://android.jlelse.eu/lazy-initialisation-whats-a-correct-implementation-64c4638561e)
     - [website 2](https://www.geeksforgeeks.org/lazy-loading-design-pattern/)
 - [observer](https://www.geeksforgeeks.org/observer-pattern-set-1-introduction/)
 - [singleton](https://www.youtube.com/watch?v=KUTqnWswPV4)
 - strategy
     - [website 1](https://www.geeksforgeeks.org/strategy-pattern-set-1)
     - [website 2](https://www.geeksforgeeks.org/strategy-pattern-set-2/?ref=rp)
 - telescoping
     - [website 1](https://howtodoinjava.com/design-patterns/creational/builder-pattern-in-java/)
     - [website 2](https://www.vojtechruzicka.com/avoid-telescoping-constructor-pattern/)