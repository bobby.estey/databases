// https://www.youtube.com/watch?v=KUTqnWswPV4
// singleton - only create a single instance
package designPatterns.singleton;

public class Single {
	static Single single = new Single();

	// Constructor is private not accessible
	private Single() {
	}

	public static Single getInstance() {
		return single;
	}
}
