package designPatterns.singleton;

public class Singleton {

	public static void main(String[] args) {

		// Single single1 = new Single();
		// Single single2 = new Single();
		@SuppressWarnings("unused")
		Single single = Single.getInstance();

		System.out.println("End Program");
	}

}
