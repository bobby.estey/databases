
public class OctalDecimailHexExample {

	public static void main(String[] args) {

		int value = 20;

		// binary - base 2 - 0 or 1
		
		// octal - base 8 - 0 -> 7
		System.out.printf("octal:       %o\n\n", value);

		// decimal - base 10 - 0 -> 9
		System.out.printf("decimal:     %d\n\n", value);

		// hexidecimal - base 16 - 0 -> f (10 = a, 11 = b, 12 = c, 13 = d, 14 = e, 15 = f
		System.out.printf("hexidecimal: %h\n\n", value);
		
		System.out.printf("octal: %o decimal: %d hexidecimal: %h", value, value, value);
	}

}