package junit;

public class CentsToDollars {

	private int cents = 0;

	public CentsToDollars() {
	}

	/** set the cents attribute */
	public void setCents(int cents) {
		this.cents = cents;
	}

	/** get the cents amount */
	public int getCents() {
		int dollars = (int) (cents / 100);
		int cnts = cents - dollars * 100;
		return cnts;
	}

	/** get the dollar amount */
	public int getDollars() {
		return (int) (cents / 100);
	}

	/** print results of both the dollar and cent amount */
	public void printResults() {
		System.out.println("That is: " + getDollars() + " dollars and " + getCents() + " cents.");
	}

	public static void main(String[] args) {

		CentsToDollars centsToDollars = new CentsToDollars();
		centsToDollars.setCents(327);
		centsToDollars.printResults();
	}
}
