# [Generics](https://docs.oracle.com/javase/tutorial/java/generics/index.html)

## Why Generics?
  - define and initialize the types up front that are type safe
  - reuse of code, using the same logic with different types, e.g. 
    - Integer, Double, String, Object
  - no more casting, double doubleNumber = (int)intNumber;
  
## Parameters
  - [Types](https://docs.oracle.com/javase/tutorial/java/generics/types.html)
  - [Type Inference](https://docs.oracle.com/javase/tutorial/java/generics/genTypeInference.html)
  - [Bounded Type Parameters](https://docs.oracle.com/javase/tutorial/java/generics/bounded.html)

  - The most commonly used type parameter names are:
    - E - Element (used extensively by the Java Collections Framework) 
    - K - Key 
    - N - Number
    - T - Type 
    - V - Value S,U,V etc. - 2nd, 3rd, 4th types

## Terminology
 - autoboxing - convert primitive to Object, e.g. int to Integer 
 - unboxing - convert Object to primitive, e.g. Integer to int
 - upperbound -
 - lowerbound -
 - wildcard -
 - erasure -
 
## Examples
 - [Example 1](Driver1.java)
 - [Example 2](Driver2.java)
 
