package generics;

import java.util.ArrayList;

class Target<T> {

	ArrayList<Integer> arrayList = new ArrayList<>();

	public Target() {
		arrayList.add(11);
		arrayList.add(12);
	}

	// if (linkedList.data == target)// found
	// return contains(target, linkedList.next); // moving to next node in list
	@SuppressWarnings("deprecation")
	boolean contains(int target, LinkedListNode<Integer> linkedList) {

		for (int i = 0; i < linkedList.size(); i++) {

			int j = new Integer((int) linkedList.get(i));
			System.out.println("j: " + j + " linkedList.get(i): " + linkedList.get(i));

			if (j == target) {
				return true;
			}

			if (arrayList.contains(linkedList.get(i))) {
				return true;
			}
		}

		return false;
	}
}