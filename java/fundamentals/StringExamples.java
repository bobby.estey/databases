package fundamentals;

public class StringExamples {

	public static void main(String[] args) {
		char data[] = { 'a', 'b', 'c' };
		String abcDEFghi = "abcDEFghi";
		String stringInteger = "64";
		String stringDouble = "64.64";
		String stringBoolean = "true";
		String upperCase = abcDEFghi.toUpperCase();
		String lowerCase = abcDEFghi.toLowerCase();

		String charArray2String = new String(data);

		System.out.println("charArray2String: " + charArray2String);
		System.out.println(abcDEFghi.substring(0, 1)); // a
		System.out.println(abcDEFghi.substring(1, 2)); // b
		System.out.println(abcDEFghi.substring(2, 3)); // c
		System.out.println(abcDEFghi.substring(1, 3)); // bc
		System.out.println(abcDEFghi.substring(3, 6)); // DEF
		System.out.println("length: " + abcDEFghi.length());
		System.out.println("indexOf('D'): " + abcDEFghi.indexOf('D'));

		System.out.println("upperCase: " + upperCase);
		System.out.println("lowerCase: " + lowerCase);
		
		if (upperCase.contains("G")) {
			System.out.println("g exists");
		} else {
			System.out.println("g does not exist");
		}

		int intValue = Integer.parseInt(stringInteger);
		double doubleValue = Double.parseDouble(stringDouble);
		boolean booleanValue = Boolean.parseBoolean(stringBoolean);
		System.out.println("intValue: " + intValue);
		System.out.println("doubleValue: " + doubleValue);
		System.out.println("booleanValue: " + booleanValue);
		
		System.out.println("End Program");
	}
}
