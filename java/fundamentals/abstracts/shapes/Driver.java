package fundamentals.abstracts.shapes;

public class Driver {
	public static void main(String[] args) {
		Square square5 = new Square("five", 5);
		Square square4 = new Square("four", 4);
		System.out.println("area5: " + square5.area(5));
		System.out.println("area4: " + square4.area(4));
		// System.out.println("equals: " + square5.equals(square4));
		square4.setName("4");
		System.out.println("area4.name: " + square4.getName());
	}
}
