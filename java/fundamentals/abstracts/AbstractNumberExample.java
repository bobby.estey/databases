// abstract class Number is the super class to all numbers, e.g.
// Integer, Double and Byte
package fundamentals.abstracts;

public class AbstractNumberExample {

	public void show(Number number) {
		System.out.println("Class: " + number.getClass().getName() + ": " + number);

		/*
		 * public void show(Integer number) { System.out.println("Integer: " + number);
		 * }
		 * 
		 * public void show(Double number) { System.out.println("Double:  " + number); }
		 * 
		 * public void show(Byte number) { System.out.println("Byte:    " + number); }
		 */
	}

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {

		AbstractNumberExample abstractNumberExample = new AbstractNumberExample();
		abstractNumberExample.show(5); // call with an Integer
		abstractNumberExample.show(5.5); // call with a Double
		abstractNumberExample.show(new Byte("64")); // call with a Byte
	}
}
