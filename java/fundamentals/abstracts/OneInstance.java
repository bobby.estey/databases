package fundamentals.abstracts;

public class OneInstance {

	public OneInstance() {
		System.out.println("OneInstance Constructor");
	}

	public static String getString() {
		return "getString";
	}

	public static void main(String[] args) {
		System.out.println("calling object: " + OneInstance.getString());
		System.out.println("Calling Class:  " + OneInstance.getString());
	}
}