package fundamentals.abstracts.animals;

public class Hound extends Dog {

	public void eat() {
		System.out.println("Hound eatting");
	}

	public void bark() {
		System.out.println("Hound barking");
	}
}
