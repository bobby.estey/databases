// Java recursive program - Towers of Hanoi
package fundamentals.recursion;

public class TowersOfHanoi {

	private static final int NUMBER_DISKS = 4;

	// original input is from, to and spare pole
	public static void towerOfHanoi(int numberDisks, char fromPole, char toPole, char sparePole) {
		if (numberDisks == 1) {
			System.out.println("Move disk 1 from pole " + fromPole + " to pole " + toPole);
			return;
		}

		// second input is from, spare and to pole
		towerOfHanoi(numberDisks - 1, fromPole, sparePole, toPole);
		System.out.println("Move disk " + numberDisks + " from pole " + fromPole + " to pole " + toPole);

		// third input is spare, to and from pole
		towerOfHanoi(numberDisks - 1, sparePole, toPole, fromPole);
	}

	public static void main(String args[]) {
		towerOfHanoi(NUMBER_DISKS, 'A', 'C', 'B');
	}
}
