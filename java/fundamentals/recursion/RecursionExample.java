// https://www.geeksforgeeks.org/program-to-efficiently-calculate-ex/

package fundamentals.recursion;

public class RecursionExample {

	public static double exponential(int n, float x) {

		double sum = 1;

		for (int i = n - 1; i > 0; --i)
			sum = 1 + x * sum / i;

		return sum;
	}

	public static int recursiveFactorial(int n) {
		System.out.println("recursiveFactorial(" + n + ")");
		
		// base case - keep calling the method until a return occurs
		if (n == 1) {
			return 1;
			
			// otherwise keep calling the direct recursive method
		} else {
			int i = recursiveFactorial(n - 1);
			System.out.println("return n * i : " + (n * i) + "\t n: " + n + "\t i: " + i);
			return n * i;
		}
	}

	public static void main(String[] args) {
		int factorial = RecursionExample.recursiveFactorial(5);
		System.out.println("factorial: " + factorial);
		System.out.println("e^x = " + exponential(10, 1));
	}
}