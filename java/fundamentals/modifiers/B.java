package fundamentals.modifiers;

public class B {

	public static void main(String[] args) {
		A a = new A();

		System.out.println(a.getMyPrivate());
		System.out.println(a.myDefault);
		System.out.println(a.myProtected);
		System.out.println(a.myPublic);
	}
}
