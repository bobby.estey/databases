package fundamentals.properties;

import java.util.Properties;

public class Environment {

	public static void main(String[] args) {

		// retrieving the System properties that are currently on the Java Virtual
		// Machine (JVM)
		Properties properties = System.getProperties();
		properties.list(System.out);

		// prints the environment variables
		String fileSeparator = properties.getProperty("file.separator");

		System.out.println("\n\n-----------------------------------------\n\n");
		System.out.println("File Separator: " + fileSeparator);

		System.out.println("Java Version: " + System.getProperty("java.version"));
		System.out.println("Java Runtime Version: " + System.getProperty("java.runtime.version"));
		System.out.println("Java Home: " + System.getProperty("java.home"));
		System.out.println("Java Vendor: " + System.getProperty("java.vendor"));
		System.out.println("Java Vendor URL: " + System.getProperty("java.vendor.url"));
		System.out.println("Java Class Path: " + System.getProperty("java.class.path") + "\n");
	}
}