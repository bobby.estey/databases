// https://docstore.mik.ua/orelly/java-ent/jnut/ch03_12.htm
// Anonymous classes have no name, instantiated using the "new" operator and are an expression
// Anonymous classes allow defining a class in usually one line of code and there is only one instance of the class
package fundamentals;

public class AnonymousClassExample {

	// method returns an anonymous Class of type String
	public static String anonymousClass() {

		// instantiates a new object without a Class name
		return new String("anonymous Class object");
	}

	public static void main(String[] args) {
		System.out.println(AnonymousClassExample.anonymousClass());
	}
}