package fundamentals.binding;

public class StaticVSDynamicBinding {

	public static class TopClass {
		void print() {
			System.out.println("print in TopClass.");
		}
	}

	public static class MiddleClass extends TopClass {
		// void print() {
		// System.out.println("print in MiddleClass.");
		// }
	}

	public static class SubClass extends MiddleClass {

		@Override
		void print() {
			System.out.println("print in SubClass.");
		}

		void hierarchy() {
			super.print(); // super = MiddleClass
			this.print(); // this = class
		}
	}

	public static void main(String[] args) {
//		MiddleClass MiddleClass = new MiddleClass();
		// MiddleClass SubClassFake = new SubClass();
		SubClass SubClassReal = new SubClass();

		// MiddleClass.print();
		// SubClassFake.print();
		// SubClassReal.print();
		SubClassReal.hierarchy();
	}
}