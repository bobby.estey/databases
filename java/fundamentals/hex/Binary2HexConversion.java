// https://www.mkyong.com/java/how-to-convert-hex-to-ascii-in-java/
// methods - are functions
// static - class method
// https://stackoverflow.com/questions/35746515/hex-to-string-and-back-value-not-matching
package fundamentals.hex;

public class Binary2HexConversion {

	public static void convertBinary2Hexadecimal(String binary) {

		// Integer is a class and has a method called parseInt that accepts a String and
		// convert to BASE 2
		// binary is base 2 (0, 1), decimal is base 10 (0 - 9) and hexadecimal (0 -
		// f=15) is base 16
		int decimal = Integer.parseInt(binary, 2); // get the binary string and convert from base 2 to base 10

		// convert the decimal number into hexadecimal (16) and return the hexadecimal
		String hexaDecimal = Integer.toString(decimal, 16);

		System.out.println("binary: " + binary + " hexaDecimal: " + hexaDecimal);
	}

	public static void main(String[] args) {

		Binary2HexConversion.convertBinary2Hexadecimal("11101");

		Binary2HexConversion.convertBinary2Hexadecimal("11101");
		Binary2HexConversion.convertBinary2Hexadecimal("1010");
		Binary2HexConversion.convertBinary2Hexadecimal("1011");
	}
}
