package fundamentals.interfaces;

public class ConverterSwitch implements Converter {

	@Override
	public String convertMonth(int monthNumber) {

		switch (monthNumber) {
		case 1:
			System.out.println("January");

		case 2:
			System.out.println("February");

		case 3:
			System.out.println("March");

		case 4:
			return ("April");

		case 5:
			return ("May");

		case 6:
			return ("June");

		case 7:
			return ("July");

		case 8:
			return ("August");

		case 9:
			return ("September");

		case 10:
			return ("October");

		case 11:
			return ("November");

		case 12:
			return ("December");

		default:
			return ("You have entered an invalid number. You must enter a number between 1 and 12. Goodbye.");
		}
	}

	@Override
	public String convertDay(int dayNumber) {
		// TODO Auto-generated method stub
		return null;
	}
}
