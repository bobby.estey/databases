package fundamentals.interfaces.aircraft;

public class Wings implements Wings2Electronics {

	private int flaps = 0;

	@Override
	public int getFlaps() {
		return flaps;
	}

	@Override
	public void setFlaps(int flaps) {
		this.flaps = flaps;
	}
}