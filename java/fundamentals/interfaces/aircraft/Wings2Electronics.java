package fundamentals.interfaces.aircraft;

public interface Wings2Electronics {

	public int getFlaps();
	public void setFlaps(int flaps);
}
