package fundamentals.interfaces;

public class Parent implements InterfaceA, InterfaceB {
	// Interface A methods

	public String getA1() {
		return "parent.A1";
	}

	public String getA2() {
		return "parent.A2";
	}

	// Interface B methods

	public String getB1() {
		return "parent.B1";
	}

	public String getB2() {
		return "parent.B2";
	}
}
