package fundamentals.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class DrawImage {

	// c:\\users\\rwe001\\mypicInput.png
	private static final String INPUT_FILE = "files/logo.png";
	private static final String OUTPUT_FILE = "files/logoOut.png";

	public static void main(String[] args) {

		try {
			// c:\\users\\rwe001\\mypic.png
			BufferedImage originalImage = ImageIO.read(new File(INPUT_FILE));
			ImageIO.write(originalImage, "png", new File(OUTPUT_FILE));

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}