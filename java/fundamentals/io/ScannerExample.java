package fundamentals.io;

import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) {

		// Scanner is the Type (Class) that is being created
		// keyboard is the object that is returned from the Constructor
		// new - means a Constructor is following and being called
		// Scanner() is the Constructor which always returns an object
		// System.in is the keyboard
		Scanner keyboard = new Scanner(System.in);

		System.out.print("Enter something using the keyboard: ");

		System.out.println("You entered: " + keyboard.nextLine());

		System.out.print("Input first number: ");
		int num1 = keyboard.nextInt();

		System.out.print("Input second number: ");
		int num2 = keyboard.nextInt();

		System.out.println(num1 + " x " + num2 + " = " + num1 * num2);

		keyboard.close();
	}
}