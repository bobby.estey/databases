package fundamentals;

import java.util.Scanner;

// Accept a sentence from the user, via the keyboard. Move the first word of 
// the sentence to the end of the sentence. Remove the capitalization from 
// that word and capitalize the new first word of the sentence.
public class StringManipulation2 {
	public static void main(String[] args) {

		// gets input from the keyboard, e.g. System.in
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter a sentence.");

		// get the input from the user and put into a String in our case we are going to
		// call the object sentence
		String sentence = keyboard.nextLine();

		// returns the index of the first space in the String
		int firstSpaceindex = sentence.indexOf(" ");

		// get the first word
		String firstWord = sentence.substring(0, firstSpaceindex);

		// gets the sentence without the firstWord and create a new object called
		// newSentence
		// now get the first space index plus 1 and the remaining characters of the
		// string
		// ignoring the first word and put into a new object called newSentence

		// so if we have input as "one two three" we are going to get the 4th index to
		// the end
		// and put into the newSentence which is now "two three"
		String newSentence = sentence.substring(firstSpaceindex + 1, sentence.length());

		// then all we need to do is add the first word to the end of the new Sentence
		// += means concatenate to the end of newSentence a space and the original first
		// word.
		// prior to concatenating the firstWord to the end of the sentence change to
		// lowercase.
		newSentence += " " + firstWord.toLowerCase();

		// change the first character to uppercase
		newSentence.substring(0, 1).toUpperCase();

		newSentence.substring(0).toUpperCase();

		System.out.println(newSentence);

		keyboard.close();
	}
}
