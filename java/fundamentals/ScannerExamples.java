// https://mkyong.com/java/java-how-to-create-and-write-to-a-file/
package fundamentals;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ScannerExamples {

	// String fileName = "c://lines.txt"; // windoze format
	// String fileName = "/home/rwe001/git/cv64/java/maven/jdk14/README.md";
	public static String getFileName() {

		// System.in - keyboard
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter Filename to Open: ");
		String fileName = keyboard.nextLine(); // nextLine is looking for a \n <cr>
		keyboard.close();

		return fileName;
	}

	public static void writeFile(String fileName) {

		Charset utf8 = StandardCharsets.UTF_8; // most popular standard for computers
		List<String> list = Arrays.asList("row 1", "row 2", "row 3");

		try {
			// If the file doesn't exists, create and write to it
			// If the file exists, truncate (remove all content) and write to it
			Files.write(Paths.get(fileName), list, utf8);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	public static void readFile(String fileName) {
		try (Scanner readfile = new Scanner(new File(fileName))) {

			while (readfile.hasNext()) {
				System.out.println(readfile.nextLine());
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	// calls static methods (Class methods)
	public static void main(String[] args) {

		String fileName = getFileName();
		writeFile(fileName);
		readFile(fileName);

		System.out.println("End Program");
	}
}
