/**
 * this equals Warrior (this always equals the Class strength (initial value =
 * 75) health (initial value = 100) stamina (initial value = 100) speed (initial
 * value = 50) attackPower (initial value = 10) shieldStrength (initial value =
 * 100)
 */
package fundamentals.ooo.game;

public class Warrior extends Human {

	private int shieldStrength = 0;

	public Warrior(String name, int shieldStrength) {
		this.setName(name);
		this.setStrength(75);
		this.setHealth(100);
		this.setStamina(100);
		this.setSpeed(50);
		this.setAttackPower(10);
		this.setShieldStrength(shieldStrength);
	}

	public int getShieldStrength() {
		return shieldStrength;
	}

	public void setShieldStrength(int shieldStrength) {
		this.shieldStrength = shieldStrength;
	}
}
