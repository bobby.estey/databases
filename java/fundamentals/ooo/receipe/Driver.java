package fundamentals.ooo.receipe;

public class Driver {

	// main only has ONE PURPOSE - tells the Java Virtual Machine (JVM) where to
	// start
	// only put required implementation in the main method
	// public - Class can access, void returns nothing
	// static - Class method or attribute
	// java Receipe -Dtime=5 -Dcost=5.4
	// args[0] = 5 args[1]=5.4
	public static void main(String[] args) {

		// Receipe - Class
		// receipe - object
		// new - means a Constructor is following
		// Receipe() - Constructor, in this case a no argument Constructor
		Receipe pie = new Receipe();
		pie.setCost(10);
		pie.setTemperature(350);
		pie.setTime(1);

//		Receipe cake = new Receipe(20.99, 2, 350);

		System.out.println(pie.getCost());
		System.out.println("End Program");
	}
}
