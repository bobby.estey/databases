/**
 * Driver Class instructs the Java Virtual Machine (JVM) where to start the
 * minimal amount of code should be in this class.  Driver Class has the main method
 * 
 * https://www.navysite.de/cruisebooks/cv64-78/310.htm
 * Cruise (1978 - 1979) Task Force 77 / Task Group 77.7
 * 
 * http://www.seaforces.org/usnair/CVW/Carrier-Air-Wing-9.htm
 * 
 */
package fundamentals.ooo.design;

import java.util.ArrayList;
import java.util.List;

public class Navy {

	private static List<Air> squadrons = new ArrayList<>();
	private static List<Ship> ships = new ArrayList<>();
	private static List<Land> forces = new ArrayList<>();

	private static void addAircraft() {

		// http://www.seaforces.org/usnair/VF/Fighter-Squadron-211.htm
		Air vf211 = new Air("CVW9", "VF211", "Checkmates", "F14D");

		// http://www.seaforces.org/usnair/VF/Fighter-Squadron-24.htm
		Air vf24 = new Air("CVW9", "VF24", "Renegades", "F14D");

		// http://www.seaforces.org/usnair/HS/Helicopter-Anti-Submarine-Squadron-6.htm
		Air hs6 = new Air("CVW9", "HS6", "Indians", "H3");

		// http://www.seaforces.org/usnair/VA/Attack-Squadron-146.htm
		Air va146 = new Air("CVW9", "VA146", "Blue Diamonds", "A7E");

		// http://www.seaforces.org/usnair/VA/Attack-Squadron-147.htm
		Air va147 = new Air("CVW9", "VA147", "Argonauts", "A7E");

		// https://en.wikipedia.org/wiki/VA-165_(U.S._Navy)
		Air va165 = new Air("CVW9", "VA165", "Boomers", "A6E");

		// http://www.seaforces.org/usnair/VAW/Carrier-Airborne-Early-Warning-Squadron-126.htm
		Air vaw126 = new Air("CVW9", "VAW126", "Seahawks", "E2C");

		// https://en.wikipedia.org/wiki/VAQ-132
		Air vaw132 = new Air("CVW9", "VAW132", "Scorpions", "EA6B");

		// https://en.wikipedia.org/wiki/VFP-63
		Air vfp63 = new Air("CVW9", "VFP63", "Eyes of the Fleet", "RF8G");

		// https://en.wikipedia.org/wiki/VQ-1
		Air vq1 = new Air("CVW9", "VQ1", "World Watchers", "EA3B");

		// https://www.globalsecurity.org/military/agency/navy/vs-37.htm
		Air vs37 = new Air("CVW9", "VS37", "Rooster Tails / Sawbucks", "S3B");

		squadrons.add(vf211);
		squadrons.add(vf24);
		squadrons.add(hs6);
		squadrons.add(va146);
		squadrons.add(va147);
		squadrons.add(va165);
		squadrons.add(vaw126);
		squadrons.add(vaw132);
		squadrons.add(vfp63);
		squadrons.add(vq1);
		squadrons.add(vs37);
	}

	private static void addShips() {
		// https://www.navysite.de/cruisebooks/cv64-78/index.html
		// https://en.wikipedia.org/wiki/USS_Constellation_(CV-64)
		Ship cv64 = new Ship("CV64", "Constellation");

		// https://en.wikipedia.org/wiki/USS_Kansas_City_(AOR-3)
		Ship aor3 = new Ship("AOR3", "Kansas City");

		// https://en.wikipedia.org/wiki/USS_Sterett_(CG-31)
		Ship cg31 = new Ship("CG31", "Sterett");

		// https://en.wikipedia.org/wiki/USS_Waddell
		Ship ddg24 = new Ship("DDG24", "Waddell");

		ships.add(cv64);
		ships.add(aor3);
		ships.add(cg31);
		ships.add(ddg24);
	}

	public static void main(String[] args) {

		addAircraft();
		addShips();

		// TaskForce - Class (Type)
		// taskForce - object (instance of the Type)
		// new - a Constructor is following
		// TaskForce() - Constructor - create and return an object
		TaskForce tf77 = new TaskForce(7, squadrons, ships, forces);
		System.out.println(tf77.toString());
	}
}
