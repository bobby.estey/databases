/**
 * Naval Sea Forces, e.g. Surface and SubSurface
 * 
 * Surface JavaBean - JavaBean is defined as attributes, Constructors and getters / setters defined
 */
package fundamentals.ooo.design;

public class Ship {

	private String designation = "";
	private String name = "";

	public Ship(String designation, String name) {

		this.designation = designation;
		this.name = name;
	}

	public Ship() {
		this("none", "n/a"); // calls the 2 argument Constructor with the values
	}

	/**
	 * @return the attribute values
	 */
	public String toString() {
		String string = "";

		string = "Designation: " + designation + " Name: " + name;

		return string;
	}

	// getters and setters are also known as (AKA) accessors (get) and mutators
	// (set)
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
