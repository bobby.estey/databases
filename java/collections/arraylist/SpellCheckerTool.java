package collections.arraylist;
// https://mkyong.com/java/how-to-read-file-from-java-bufferedreader-example/
// https://mkyong.com/java/how-to-loop-arraylist-in-java/
// https://mkyong.com/java/how-to-convert-list-to-set-arraylist-to-hastset/
// https://mkyong.com/java/java-how-to-get-the-first-item-from-set/

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is a basic Spell Checker. Using a dictionary provided, it allows
 * for all words in a given text to be checked against that dictionary. If a
 * word is in the text but not in the dictionary it is misspelled.
 */
public class SpellCheckerTool {
	// all words in the dictionary should be stored in both the
	// hashedDictionary and the listDictionary.
	private HashSet<String> hashedDictionary = null;
	private ArrayList<String> listDictionary = null;

	/**
	 * Instantiate this SpellCheckerTool.
	 * 
	 * @param dictionaryFileName the dictionary to use for testing spellings
	 */
	public SpellCheckerTool(String dictionaryFileName) {
		// initialize both dictionaries
		listDictionary = buildArrayList(dictionaryFileName);
		hashedDictionary = new HashSet<String>(listDictionary);
	}

	/**
	 * Return a list of all misspelled words in the text file. A word is misspelled
	 * if it is in the text file but not in the dictionary. This method removes all
	 * leading and trailing whitespace and punctuation from the words in the text
	 * 
	 * @param textFileName the name of the text file
	 * @return a list of all misspelled words in this text file.
	 */
	public Set<String> hashCheckSpellings(String textFileName) {
		// Read in the text and return a list of all words in it that are
		// not in the hashedDictionary

		HashSet<String> misspelledHash = new HashSet<>();
		String line = "";

		try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(textFileName))) {

			while ((line = bufferedReader.readLine()) != null) {

				String[] words = line.split(" ");

				for (int i = 0; i < words.length; i++) {
					words[i] = words[i].replaceAll("\\p{Punct}", "").toLowerCase().trim();
					if (!hashedDictionary.contains(words[i])) {
						if (!misspelledHash.contains(words[i])) {
							misspelledHash.add(words[i]);
						}
					}
				}
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		return misspelledHash;
	}

	/**
	 * Return a list of all misspelled words in the text file. A word is misspelled
	 * if it is in the text file but not in the dictionary. This method removes all
	 * leading and trailing whitespace and punctuation from the words in the text
	 * 
	 * @param textFileName the name of the text file
	 * @return a list of all misspelled words in this text file.
	 */
	public List<String> listCheckSpellings(String textFileName) {
		// Read in the text and return a list of all words in it that are
		// not in the listDictionary

		ArrayList<String> grimmsArrayList = buildArrayList(textFileName);
		ArrayList<String> misspelledList = new ArrayList<>();

		for (String element : grimmsArrayList) {
			element = element.replaceAll("\\p{Punct}", "").toLowerCase().trim();

			if (!listDictionary.contains(element)) {
				if (!misspelledList.contains(element)) {
					misspelledList.add(element);
				}
			}
		}

		return misspelledList;
	}

	/**
	 * get the filename of the text to check for misspellings and load into an array
	 * list of words
	 * 
	 * @param filename
	 * @return array list of words
	 */
	private ArrayList<String> buildArrayList(String filename) {

		ArrayList<String> arrayList = new ArrayList<>();
		String line = "";

		try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(filename))) {

			while ((line = bufferedReader.readLine()) != null) {

				String[] words = line.split(" ");

				for (int i = 0; i < words.length; i++) {
					arrayList.add(words[i]);
				}
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		return arrayList;
	}
}
