package collections.arraylist;
// https://mkyong.com/java/how-to-loop-arraylist-in-java/
// https://beginnersbook.com/2014/08/how-to-iterate-over-a-sethashset/

import java.util.List;
import java.util.Set;

/**
 * Taking in the path to a dictionary and a text file as command line arguments,
 * CheckSpellings uses SpellCheckerTool to identify all misspelled words in the
 * text file. The dictionary is used to check the spelling of all words in the
 * text file. This program benchmarks the time it takes to evaluate the entire
 * text using two methods of storing the dictionary: a HashSet and an ArrayList.
 */
public class CheckSpellings {
	/**
	 * Perform the spell-checking analysis. The path to the dictionary and text file
	 * are passed in as command line arguments.
	 * 
	 * @param args the 0th argument is the path to the dictionary. The 1st is the
	 *             path to the text file.
	 */
	public static void main(String[] args) {
		// Instantiate a SpellChecker that uses dictionary.txt

		// Benchmark the time it takes to run each of hashCheckSpellings()
		// and listCheckSpellings on the input text shortGrimms.txt, and
		// print out the time it took for each along with the number of
		// misspelled words found.

		long startTime = System.currentTimeMillis();
		long stopTime = 0;

		String dictionary = args[0];
		String filename = args[1];

		System.out.println("dictionary path:\t" + dictionary);
		System.out.println("filename path:\t\t" + filename);
		System.out.println("Start Program:\t\t" + startTime);

		// build the dictionary and collections (ArrayList and HashSet)
		System.out.println("Build Dictionary:\t" + System.currentTimeMillis());
		SpellCheckerTool spellCheckerTool = new SpellCheckerTool(dictionary);

		// pass in the filename to load into an ArrayList and check against misspelled
		// words
		System.out.println("Build ArrayList:\t" + System.currentTimeMillis());
		List<String> listMisspelled = spellCheckerTool.listCheckSpellings(filename);

		// pass in the filename to load into an HashSet and check against misspelled
		// words
		System.out.println("Build HashSet:\t\t" + System.currentTimeMillis());
		Set<String> hashMisspelled = spellCheckerTool.hashCheckSpellings(filename);

		stopTime = System.currentTimeMillis();
		System.out.println("Stop Program:\t\t" + stopTime);

		System.out.println("Total Time:\t\t" + (stopTime - startTime));

		System.out.println("\nMispelled Words ArrayList");
		for (String word : listMisspelled) {
			System.out.print(word + " ");
		}

		System.out.println("\nMispelled Words HashMap");
		for (String word : hashMisspelled) {
			System.out.print(word + " ");
		}
	}
}
