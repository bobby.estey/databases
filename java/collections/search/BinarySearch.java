// https://www.geeksforgeeks.org/binary-search/
package collections.search;

public class BinarySearch {

	private static int array[] = { 2, 3, 5, 8, 13, 20, 40 };
	private static final int ELEMENT_2_FIND = 13;

	// Recursively cycles through the array until found or no elements found
	// Returns index of element else return -1 if not found
	private static int search(int left, int right) {

		if (right >= left) {
			int center = left + (right - left) / 2;

			// element in the center
			if (array[center] == ELEMENT_2_FIND)
				return center;

			// element in left subarray
			if (array[center] > ELEMENT_2_FIND)
				return search(left, center - 1);

			// element in right subarray
			return search(center + 1, right);
		}

		// element not in the array
		return -1;
	}

	public static void main(String[] args) {

		int result = BinarySearch.search(0, array.length - 1);

		if (result == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element at index: " + result);
	}
}