package collections.comparator;

public class Car implements Comparable<Car> {

	private String make = "";
	private String model = "";
	private int year = 0;
	private int inventory = 0;

	public Car(String make, String model, int year, int inventory) {
		this.make = make;
		this.model = model;
		this.year = year;
		this.inventory = inventory;
	}

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public String toString() {
		return "Cars [ make is " + make + " model is " + model + " year is " + year + " inventory is " + inventory
				+ " ]";
	}

	@Override
	public int compareTo(Car c) {

		if (c.getInventory() > this.inventory) {
			return -1;
		} else if (c.getInventory() < this.inventory) {
			return 1;
		} else {
			return 0;
		}
	}
}