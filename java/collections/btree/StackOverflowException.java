package collections.btree;

public class StackOverflowException extends RuntimeException {

	private static final long serialVersionUID = -9194793735069563146L;

	public StackOverflowException() {
		super();
	}

	public StackOverflowException(String message) {
		super(message);
	}
}