// https://www.geeksforgeeks.org/selection-sort/
package collections.sort;

public class SelectionSort {

	private static int array[] = { 64, 25, 12, 22, 11 };

	public static void sort() {

		int arrayLength = array.length;

		// One by one move boundary of unsorted subarray
		for (int i = 0; i < arrayLength - 1; i++) {

			// Find the minimum element in unsorted array
			int minimumElement = i;
			for (int j = i + 1; j < arrayLength; j++)
				if (array[j] < array[minimumElement])
					minimumElement = j;

			// Swap the found minimum element with the first element
			int temp = array[minimumElement];
			array[minimumElement] = array[i];
			array[i] = temp;
		}
	}

	// Prints the array
	public static void printarray() {
		for (int i = 0; i < array.length; ++i)
			System.out.print(array[i] + " ");
		System.out.println();
	}

	public static void main(String args[]) {

		SelectionSort.sort();
		System.out.println("Sorted array");
		SelectionSort.printarray();
	}
}
