// https://mkyong.com/java/how-to-read-file-from-java-bufferedreader-example/
// https://www.geeksforgeeks.org/selection-sort/
package collections.sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Sorter {

	private ArrayList<Integer> arrayList = new ArrayList<>();

	public Sorter(String filename) {
		readFile(filename);
	}

	/**
	 * getFilename method prompts the user for a filename
	 * 
	 * @return the filename entered by the user
	 */
	public String getFilename() {
		String filename = "";

		// System.in is the keyboard
		Scanner keyboard = new Scanner(System.in);

		System.out.print("Enter the Filename: ");
		filename = keyboard.nextLine();

		keyboard.close();

		return filename;
	}

	/**
	 * readFile method reads a file
	 * 
	 * @param filename the filename to read
	 */
	public void readFile(String filename) {

		StringBuilder stringBuilder = new StringBuilder();
		String line = "";

		try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(filename))) {

			while ((line = bufferedReader.readLine()) != null) {

				String[] integers = line.split(" ");

				for (int i = 0; i < integers.length; i++) {
					arrayList.add(Integer.parseInt(integers[i]));
				}

				System.out.println(line);
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		System.out.println(stringBuilder);
	}

	/**
	 * remove duplicate elements in an ArrayList
	 */
	public void removeDuplicates() {
		ArrayList<Integer> tempArrayList = new ArrayList<>();

		for (Integer element : arrayList) {
			if (!tempArrayList.contains(element)) {
				tempArrayList.add(element);
			}
		}

		arrayList = tempArrayList;
	}

	/**
	 * sorts the arrayList and populates an array
	 * 
	 * @return sorted arraylist as an array
	 */
	public int[] sortArraylist2Array() {

		int[] integers = new int[arrayList.size()];

		for (int i = 0; i < arrayList.size(); i++) {
			integers[i] = arrayList.get(i);
		}

		int lengthIntegersArray = integers.length;

		for (int i = 0; i < lengthIntegersArray - 1; i++) {
			// Find the maximum element in unsorted array
			int index = i;
			for (int j = i + 1; j < lengthIntegersArray; j++)
				if (integers[j] > integers[index])
					index = j;

			// Swap the found maximum element with the first
			// element
			int temp = integers[index];
			integers[index] = integers[i];
			integers[i] = temp;
		}

		return integers;
	}

	/**
	 * prints the elements in an arraylist
	 * 
	 * @param integers
	 */
	public void print(int[] integers) {
		for (int i = 0; i < integers.length; i++) {
			System.out.print(integers[i] + " ");
		}
	}

	public static void main(String[] args) {
		Sorter sorter = new Sorter("/home/rwe001/a.txt");
		sorter.removeDuplicates();
		int[] integers = sorter.sortArraylist2Array();
		sorter.print(integers);
		System.out.println("\nEnd Program");
	}
}
