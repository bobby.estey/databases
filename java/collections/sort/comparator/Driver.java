// Comparator - comparing instances of different types.

package collections.sort.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Driver {

	public static void main(String[] args) {

		// create a list of type Person
		List<Person> list = new ArrayList<Person>();

		// create an object of type Person and add to list in the following order
		list.add(new Person(3, "Rebby"));
		list.add(new Person(1, "Bobby"));
		list.add(new Person(2, "Ester"));

		System.out.println("\nUnsorted List:\n");
		for (int i = 0; i < list.size(); i++)
			System.out.println(list.get(i));

		// call the Collections class, sort method and pass in the list
		// does not return a new list because we have a reference to the list
		// NOTE:  WE ARE SORTING BY A DIFFERENT CLASS
		Collections.sort(list, new SortById());

		System.out.println("\nSorted by id:\n");
		for (int i = 0; i < list.size(); i++)
			System.out.println(list.get(i));

		// call the Collections class, sort method and pass in the list
		// does not return a new list because we have a reference to the list
		// NOTE:  WE ARE SORTING BY A DIFFERENT CLASS
		Collections.sort(list, new SortByName());

		System.out.println("\nSorted by name:\n");
		for (int i = 0; i < list.size(); i++)
			System.out.println(list.get(i));
	}
}
