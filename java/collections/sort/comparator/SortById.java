package collections.sort.comparator;

import java.util.Comparator;

public class SortById implements Comparator<Person> {

	// Used to sort by ID - compareTo method that compares the id
	// person.id of the Person (two Classes being passed in of Person) that is being
	// passed in to the Class (this) and return
	public int compare(Person person1, Person person2) {

		// return negative or positive or zero
		return person1.id - person2.id;
	}
}
