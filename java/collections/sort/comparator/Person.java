package collections.sort.comparator;

public class Person {

	public int id = 0;
	public String name = "";

	public Person(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public String toString() {
		return this.id + " " + this.name;
	}
}
