package collections.tree;

import javax.swing.SwingUtilities;

public class GraphWindowDriver {
	/**
	 * Displays a GUI interface for viewing data
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new GraphWindow().setVisible(true));
	}
}
