package restful;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RESTfulExample {

	// path to the Server the request is made
	// private static final String JSON_PATH = "https://cv64.us/cv64/database/public/selectAll2.php";
	private static final String JSON_PATH = "https://api.publicapis.org/entries";
	
	/** JSON Object
	 * @see <a href="https://en.wikipedia.org/wiki/JSON">JSON Object</a>
	 */
	private static JSONObject jsonObject = null;

	/**
	 * read each character one at a time, convert the integer value into a String.
	 * Build a String of characters and the return as a single String
	 * 
	 * @param reader
	 * @return the String of characters
	 * @throws IOException
	 */
	private static String readAll(Reader reader) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		int character;
		while ((character = reader.read()) != -1) {
			stringBuilder.append((char) character);
		}

		return stringBuilder.toString();
	}

	/**
	 * read website's URL and build JSON Object
	 * 
	 * @param url
	 * @throws IOException
	 * @throws JSONException
	 */
	private static void readJsonFromUrl(String url) throws IOException, JSONException {

		InputStream inputStream = null;

		try {
			inputStream = new URL(url).openStream();

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream, Charset.forName("UTF-8")));

			String jsonText = readAll(bufferedReader);

			// strip off array to get only one record
//			jsonText = jsonText.substring(1, jsonText.length() - 2);

			// get json record
			jsonObject = new JSONObject(jsonText);

		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			inputStream.close();
		}
	}

	/**
	 * print json object contents
	 * 
	 * @param jsonObject
	 */
	private static void toString(JSONObject jsonObject) {
		System.out.println("\n\n");
		System.out.println("jsonObject.toString(): " + jsonObject.toString());
		System.out.println("\n\n");
		System.out.println("entries:               " + jsonObject.get("entries"));
		System.out.println("count:                 " + jsonObject.get("count"));
//		System.out.println("jsonObject.id: " + (String) jsonObject.get("id") + "\njsonObject.firstName: "
//				+ (String) jsonObject.get("firstName") + "\njsonObject.lastName: "
//				+ (String) jsonObject.get("lastName"));
		
		  JSONArray apis = jsonObject.getJSONArray("entries");
		  for (int i = 0; i < apis.length(); i++) {
			  JSONObject api = apis.getJSONObject(i);
			  
			  System.out.println("Description: " + (String) api.get("Description"));
		  }
	}

	public static void main(String[] args) throws IOException, JSONException {
		RESTfulExample.readJsonFromUrl(JSON_PATH);
		RESTfulExample.toString(jsonObject);
		System.out.println("End Program");
	}
}