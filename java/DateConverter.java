import java.util.Scanner; // imports bring in Code from other files

public class DateConverter {

	// main method - instructs the Java Virtual Machine (JVM) where to start
	public static void main(String[] args) {

		int dayNumber, monthNumber, date, year;
		String dayName, monthName;

		System.out.println("Enter 4 integers representing dayNumber monthNumber date year, "
				+ "e.g. 1 (Sun) 1 (Jan): 31 - 31st day, 2020: ");

		// Scanner - gets input from keyboard (System.in)
		Scanner keyboard = new Scanner(System.in);

		dayNumber = keyboard.nextInt();
		monthNumber = keyboard.nextInt();
		date = keyboard.nextInt();
		year = keyboard.nextInt();

		// relinquish memory, don't need anymore
		keyboard.close();

		if (dayNumber > 7) {
			System.err.println("Invalid day number: " + dayNumber + ", please enter a number from 1..7.");
			return;
		}

		if (monthNumber > 12) {
			System.err.println("Invalid month number: " + monthNumber + ", please enter a number from 1..12.");
			return;
		}

		if (date > 31) {
			System.err.println("Invalid date number: " + date + ", please enter a number from 1..31.");
			return;
		}

		// Assigning dayName

		if (dayNumber == 1)
			dayName = "Sunday";
		else if (dayNumber == 2)
			dayName = "Monday";
		else if (dayNumber == 3)
			dayName = "Tuesday";
		else if (dayNumber == 4)
			dayName = "Wednesday";
		else if (dayNumber == 5)
			dayName = "Thursday";
		else if (dayNumber == 6)
			dayName = "Friday";
		else
			dayName = "Saturday";

		// Assigning monthName

		if (monthNumber == 1)
			monthName = "January";
		else if (monthNumber == 2)
			monthName = "February";
		else if (monthNumber == 3)
			monthName = "March";
		else if (monthNumber == 4)
			monthName = "April";
		else if (monthNumber == 5)
			monthName = "May";
		else if (monthNumber == 6)
			monthName = "June";
		else if (monthNumber == 7)
			monthName = "July";
		else if (monthNumber == 8)
			monthName = "August";
		else if (monthNumber == 9)
			monthName = "September";
		else if (monthNumber == 10)
			monthName = "October";
		else if (monthNumber == 11)
			monthName = "November";
		else
			monthName = "December";

		// Remaining checks

		if ((monthNumber == 2 && (date == 30 || date == 31)) || (monthNumber == 4 && date == 31)
				|| (monthNumber == 6 && date == 31) || (monthNumber == 9 && date == 31)
				|| (monthNumber == 11 && date == 31)) {
			System.err.println(
					"Invalid date: " + monthName + ", does not have " + date + " days, please enter a valid date.");
			return;
		}

		if (monthNumber == 2 && date == 29) {
			if (!(year % 4 == 0 && year % 100 != 0))

			{
				System.err.println("Invalid date: " + year + " is not a leap year, February does not have " + date
						+ " days, please enter a valid date.");
				return;
			}

		}

		System.out.println(dayNumber + " " + monthNumber + " " + date + " " + year + " is " + dayName + " " + monthName
				+ " " + date + ", " + year);
	}
}