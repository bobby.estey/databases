import java.util.Scanner;

public class DataVisualization {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner console = new Scanner(System.in);
		String title = "";
		String headerColumn1 = "";
		String headerColumn2 = "";
		String dataPoint = "";
		String firstWord = "";
		String secondWord = "";
		int commaLocation = 0;
		int count = 0;
		int numberCommas = 0;
		boolean inputDone = false;

		System.out.println("Enter a title for the data: ");
		title = console.nextLine();

		System.out.println("You entered: " + title);

		System.out.println("enter the column 1 header: ");
		headerColumn1 = console.nextLine();
		System.out.println("You entered: " + headerColumn1);

		System.out.println("Enter the column 2 header: ");
		headerColumn2 = console.nextLine();
		System.out.println("You entered: " + headerColumn2);

		while (!inputDone) {
			System.out.println("Enter a data point (-1 to stop input): ");
			dataPoint = console.nextLine();
			numberCommas = numberOfCommas(dataPoint);

			// If string does not contain a comma and is not quit then the string is not
			// formatted
			while ((dataPoint.indexOf(',') == -1) && (dataPoint.equals("-1") == false)) {
				System.out.println("Error: No comma in string!\n");
				System.out.println("Enter input string: ");
				dataPoint = console.nextLine();

				if (count > 1) {
					System.out.println("Too many commas in data point:");
					System.out.println("Enter a data point (-1 to stop input): ");
					dataPoint = console.nextLine();// fix else if so that it runs
				} // end if statement
			}
//			if (dataPoint.equals("-1")) {
//				inputDone = true;
//				System.out.println("ending program");
//			} // end of -1
//			else {
//				commaLocation = dataPoint.indexOf(',');
//
//				firstWord = dataPoint.substring(0, commaLocation);
//				firstWord = firstWord.replace(" ", "");
//
//				secondWord = dataPoint.substring(commaLocation + 1, dataPoint.length());
//				secondWord = secondWord.replace(" ", "");
//				System.out.println("First Word: " + firstWord);
//				int Point = Integer.parseInt(secondWord);
//				System.out.println("second word: " + Point);
//				System.out.println();
//			} // end else

			if (numberCommas == 1) {
				if (isNumber(dataPoint)) {
					System.out.println("The second string is a Valid Number: ");
				} else {
					System.err.println("The second string is an Invalid Number: ");
					System.out.println("Enter a data point (-1 to stop input): ");
					dataPoint = console.nextLine();
				}
			} else {
				System.err.println("The second string is an Invalid Number: ");
				System.out.println("Enter a data point (-1 to stop input): ");
				dataPoint = console.nextLine();
			}
		}

		console.close();
	}// end main

	// https://www.baeldung.com/java-count-chars
	public static int numberOfCommas(String string) {
		int count = 0;

		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == ',') {
				count++;
			}
		}

		return count;
	}

	/*
	 * boolean isNumber(String in) { try { Integer.parseInt(in);
	 * 
	 * }catch(Exception E) { return false; } } }
	 */

	private static boolean isNumber(String string) {

		String[] words = string.split(",");
		String firstWord = words[0];
		String secondWord = words[1];

		boolean number = true;

		for (int i = 0; i < secondWord.length(); i++) {

			if (secondWord.charAt(i) == '-') {

				if (i == 0) {
					continue;
				} else {
					number = false;
				}
			}

			if ((secondWord.charAt(i) < '0') && (secondWord.charAt(i) > 9)) {
				number = false;
			}
		}

		return number;
	}

//	private static void numberOfCommas(String dataPoint) {
//		// TODO Auto-generated method stub
//
//	}

}