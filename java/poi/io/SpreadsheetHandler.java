package poi.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import poi.config.SpreadsheetProperties;
import poi.exceptions.POIException;

public class SpreadsheetHandler {
	/** Spreadsheet Input File Stream */
	private POIFSFileSystem fileInputStream = null;

	/** Spreadsheet Output File Stream */
	private FileOutputStream fileOutputStream = null;

	/** Input Directory */
	private String inputDirectory = "";

	/** Output Directory */
	private String outputDirectory = "";

	/** Spreadsheet Output File */
	private String outputFile = "";

	/** Spreadsheet Workbook */
	private HSSFWorkbook workbook = null;

	/** Current Time as long */
	private long currentTime = 0L;

	/**
	 * PrintServiceHandler Class
	 */
	public SpreadsheetHandler() {
		/* Get the SSAPP_HOME environment variable */
		String ssappHome = System.getenv("SSAPP_HOME");

		/* if SSAPP_HOME defined, use this value as SSAPP root */
		if (ssappHome != null) {
			inputDirectory = ssappHome + SpreadsheetProperties.INPUT_DIRECTORY;
			outputDirectory = ssappHome + SpreadsheetProperties.OUTPUT_DIRECTORY;
		} else {
			inputDirectory = SpreadsheetProperties.SSAPP_HOME + SpreadsheetProperties.INPUT_DIRECTORY;
			outputDirectory = SpreadsheetProperties.SSAPP_HOME + SpreadsheetProperties.OUTPUT_DIRECTORY;
		}
	}

	/**
	 * initializeDate method gets the current time for output file settings and
	 * purges files In Accordance With
	 * PrintServiceProperties.ELAPSED_TIME_MILLISECONDS
	 * 
	 * @throws POIException
	 */
	private void initializeDate() throws POIException {
		Date date = new Date();
		this.currentTime = date.getTime();

		purgeFiles(outputDirectory);
	}

	/**
	 * initializeFiles method opens up a read only input file and prepares the
	 * output files.
	 * 
	 * @param inputFile
	 * @throws POIException
	 */
	public void initializeFiles(String inputFile) throws POIException {
		initializeInputFile(inputFile);
		initializeOutputFile();
	}

	/**
	 * initializeInput method opens up an input file.
	 * 
	 * @param inputFile
	 * @throws POIException
	 */
	public void initializeInputFile(String inputFile) throws POIException {
		try {
			fileInputStream = new POIFSFileSystem(new FileInputStream(inputFile));

			workbook = new HSSFWorkbook(fileInputStream);
		} catch (IOException exception) {
			System.err.println("SpreadsheetHandler.initializeInput: " + inputFile);
			System.err.println(exception.getMessage());

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * initializeOutput method prepares the output files.
	 * 
	 * @param outputFile
	 * @throws POIException
	 */
	public void initializeOutputFile() throws POIException {
		try {
			initializeDate();

			if (workbook == null)
				workbook = new HSSFWorkbook();

			String outputFile = outputDirectory + currentTime + ".xls";

			fileOutputStream = new FileOutputStream(outputFile);

			this.outputFile = outputFile;
		} catch (IOException exception) {
			System.err.println("initializeOutput" + exception.getMessage());

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * closeFiles method closes all files.
	 * 
	 * @throws POIException
	 */
	public void closeFiles() throws POIException {
		try {
			if (fileInputStream != null)
				fileInputStream = null;

			if (fileOutputStream != null)
				fileOutputStream.close();
		} catch (IOException exception) {
			System.err.println("closeIO" + exception.getMessage());

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * method getOutputFile returns the output file name
	 * 
	 * @return outputFile
	 */
	public String getOutputFile() {
		return outputFile;
	}

	/**
	 * method getWorkbook returns the workbook created by SSAPP
	 * 
	 * @return the workbook created by SSAPP
	 */
	public HSSFWorkbook getWorkbook() {
		return workbook;
	}

	/**
	 * method writeBook, writes spreadsheet data to file output stream.
	 * 
	 * @throws POIException
	 */
	public void writeBook() throws POIException {
		try {
			workbook.write(fileOutputStream);
		} catch (IOException exception) {
			System.out.println("writeHeader" + exception.getMessage());

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * method purgeFiles, purges old files.
	 * 
	 * @throws POIException
	 */
	private void purgeFiles(String directoryPath) throws POIException {
		File directory = null;

		try {
			directory = new File(directoryPath);
			File[] files = directory.listFiles();

			for (File filez : files) {
				String fileName = filez.getName();
				long lastModified = filez.lastModified();

				if ((currentTime - lastModified) > SpreadsheetProperties.PURGE_FILE_AGE_MILLISECONDS) {
					filez.delete();
					System.out.println("Deleting File: " + fileName);
				}
			}
		} catch (Exception exception) {
			System.err.println("SpreadsheetHandler.purgeFiles - Directory: " + directory.toString());
			System.err.println(exception.getMessage());

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * method cellValueString, returns the value of a cell
	 * 
	 * @param sheetAt the sheet number
	 * @param row     the row number
	 * @param column  the column number
	 * 
	 * @return the value of a cell
	 */
	public String cellValueString(int sheetAt, int row, int column) {
		HSSFSheet sheet = workbook.getSheetAt(sheetAt);
		HSSFRow rowSS = sheet.getRow(row);
		HSSFCell cell = rowSS.getCell(column);

		return cell.toString();
	}

	/**
	 * @return the inputDirectory
	 */
	public final String getInputDirectory() {
		return inputDirectory;
	}

	/**
	 * @return the outputDirectory
	 */
	public final String getOutputDirectory() {
		return outputDirectory;
	}
}
