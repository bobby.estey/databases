package poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class CopySpreadSheet {

	public CopySpreadSheet(String[][] data, String inputFile, String outputFile, int sheetNumber, int startRow,
			int startCol) throws IOException {

		FileInputStream fileInputStream = new FileInputStream(new File(inputFile));
		FileOutputStream fileOutputStream = new FileOutputStream(new File(outputFile));

		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet worksheet = workbook.getSheetAt(sheetNumber);

		Row row = null;
		Cell cell = null;
		int rowIndex = startRow;
		int colIndex = startCol;

		for (int i = 0; i < data.length; i++) {
			row = worksheet.createRow(rowIndex++);
			colIndex = startCol;

			for (int j = 0; j < data[i].length; j++) {
				cell = row.createCell(colIndex++);
				// cell.setCellType(Cell.CELL_TYPE_STRING);
				cell.setCellValue(data[i][j]);
			}
		}

		workbook.write(fileOutputStream);

		workbook.close();
		fileInputStream.close();
		fileOutputStream.close();
	}
}
