package poi.util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import poi.exceptions.POIException;

public class ShapeImage extends WorkbookAttributes {
	/**
	 * writes a shape in a worksheet with a background color
	 * 
	 * @param workbook
	 * @param message
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param fillColorRed
	 * @param fillColorGreen
	 * @param fillColorBlue
	 * @param fontColor
	 * @param horizontalAlignment
	 * @param verticalAlignment
	 */
	public void writeShapeColor(HSSFWorkbook workbook, String message, HSSFPatriarch patriarch, int x1, int y1, int x2,
			int y2, int fillColorRed, int fillColorGreen, int fillColorBlue, String fontColor,
			short horizontalAlignment, short verticalAlignment) {
		// concatenate a zero if the fontColor is not equal to 6 characters
		while (fontColor.length() < 6) {
			fontColor = fontColor.concat("0");
		}

		ColorConverter colorConverter = new ColorConverter();
		HSSFColor hssfColor = colorConverter.hex2poi(workbook, fontColor);

		writeShapeColor(workbook, message, patriarch, x1, y1, x2, y2, fillColorRed, fillColorGreen, fillColorBlue,
				hssfColor.getIndex(), horizontalAlignment, verticalAlignment);
	}

	/**
	 * writes a shape in a worksheet with a background color
	 * 
	 * @param workbook
	 * @param message
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param fillColorRed
	 * @param fillColorGreen
	 * @param fillColorBlue
	 */
	public void writeShapeColor(HSSFWorkbook workbook, String message, HSSFPatriarch patriarch, int x1, int y1, int x2,
			int y2, int fillColorRed, int fillColorGreen, int fillColorBlue, short fontColor, short horizontalAlignment,
			short verticalAlignment) {
		// message must contain characters
		if ((message == null) || (message.equals("")))
			message = " ";

		HSSFTextbox textbox = patriarch.createTextbox(new HSSFClientAnchor(0, 0, 0, 0, (short) x1, y1, (short) x2, y2));

		if (font == null)
			font = this.getWorkbookDefaultFont(workbook);

		textbox.setHorizontalAlignment(horizontalAlignment);
		textbox.setVerticalAlignment(verticalAlignment);

		if (!(fillColorRed == 0 && fillColorGreen == 0 && fillColorBlue == 0))
			textbox.setFillColor(fillColorRed, fillColorGreen, fillColorBlue);

		HSSFRichTextString richTextString = new HSSFRichTextString(message);
		richTextString.applyFont(font);

		font.setColor(fontColor);
		textbox.setString(richTextString);
	}

	/**
	 * writes a shape in a worksheet with a background color
	 * 
	 * @param workbook
	 * @param message
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param fillColorRed
	 * @param fillColorGreen
	 * @param fillColorBlue
	 * @param horizontalAlignment
	 * @param verticalAlignment
	 */
	public void writeShapeWithBackgroundColor(HSSFWorkbook workbook, String message, HSSFPatriarch patriarch, int x1,
			int y1, int x2, int y2, int fillColorRed, int fillColorGreen, int fillColorBlue, short horizontalAlignment,
			short verticalAlignment) {
		// writeShapeColor(workbook, message, patriarch, x1, y1, x2, y2, fillColorRed,
		// fillColorGreen, fillColorBlue,
		// new HSSFColor.BLACK().getIndex(), horizontalAlignment, verticalAlignment);
	}

	/**
	 * write a shape in a worksheet with a background color and default alignment
	 * 
	 * @param workbook
	 * @param message
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param fillColorRed
	 * @param fillColorGreen
	 * @param fillColorBlue
	 */
	public void writeShapeWithBackgroundColor(HSSFWorkbook workbook, String message, HSSFPatriarch patriarch, int x1,
			int y1, int x2, int y2, int fillColorRed, int fillColorGreen, int fillColorBlue) {
		this.writeShapeWithBackgroundColor(workbook, message, patriarch, x1, y1, x2, y2, fillColorRed, fillColorGreen,
				fillColorBlue, HSSFTextbox.HORIZONTAL_ALIGNMENT_CENTERED, HSSFTextbox.VERTICAL_ALIGNMENT_CENTER);
	}

	/**
	 * method writeShape writes a shape in a worksheet without a background color
	 * 
	 * @param workbook
	 * @param message
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public void writeShapeWithoutBackgroundColor(HSSFWorkbook workbook, String message, HSSFPatriarch patriarch, int x1,
			int y1, int x2, int y2) {
		// message must contain characters
		if ((message.equals("")) || (message == null))
			message = " ";

		writeShapeWithBackgroundColor(workbook, message, patriarch, x1, y1, x2, y2, 0, 0, 0);
	}

	/**
	 * writeImage2Shape - writes an image in a shape
	 * 
	 * @param patriarch
	 * @param workbook
	 * @param richTextString - the text to place in the cell
	 * @param imageFile      - the name of the file of the image to import
	 * @throws POIException
	 */
	public void writeImage2Shape(HSSFPatriarch patriarch, HSSFWorkbook workbook, String richTextString,
			String imageFile) throws POIException {
		try {
			// ClientAnchor sets the location in the spreadsheet where to draw the
			// image
			HSSFClientAnchor clientAnchor = new HSSFClientAnchor(0, 0, 0, 0, (short) 1, 1, (short) 2, 30);

			HSSFSimpleShape simpleShape = patriarch.createSimpleShape(clientAnchor);

			simpleShape.setFillColor(180, 180, 255);
			simpleShape.setShapeType(HSSFSimpleShape.OBJECT_TYPE_RECTANGLE);
			simpleShape.setNoFill(true);

			HSSFTextbox textbox2 = patriarch
					.createTextbox(new HSSFClientAnchor(0, 0, 900, 100, (short) 3, 3, (short) 3, 4));
			textbox2.setString(new HSSFRichTextString(richTextString));
			textbox2.setFillColor(200, 0, 0);

			patriarch.createPicture(clientAnchor, writeImage2Cell(workbook, imageFile));
		} catch (Exception exception) {
			exception.printStackTrace();

			throw new POIException(exception.getMessage());
		}
	}

	/**
	 * writeImage2Cell - writes an image in a cell
	 * 
	 * @param image
	 * @param workbook
	 * @return
	 * @throws IOException
	 */
	private int writeImage2Cell(HSSFWorkbook workbook, String image) throws IOException {
		FileInputStream fileInputStream = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		int pictureIndex = 0;
		int streamInteger = 0;

		try {
			fileInputStream = new FileInputStream(image);
			byteArrayOutputStream = new ByteArrayOutputStream();

			while ((streamInteger = fileInputStream.read()) != -1)
				byteArrayOutputStream.write(streamInteger);
			pictureIndex = workbook.addPicture(byteArrayOutputStream.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG);
		} finally {
			if (fileInputStream != null)
				fileInputStream.close();
			if (byteArrayOutputStream != null)
				byteArrayOutputStream.close();
		}
		return pictureIndex;
	}
}
