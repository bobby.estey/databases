package poi.util;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import poi.config.SpreadsheetProperties;

/**
 * WorkbookAttributes
 */
public class WorkbookAttributes {
	/** font */
	protected HSSFFont font = null;

	/** alignment of the font */
	protected boolean alignmentSet = false;

	public WorkbookAttributes() {
	}

	/**
	 * returns the default font settings of the workbook
	 * 
	 * @param workbook
	 * @return
	 */
	protected HSSFFont getWorkbookDefaultFont(HSSFWorkbook workbook) {
		font = workbook.createFont();
		// font.setColor(new HSSFColor.WHITE().getIndex());
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName(SpreadsheetProperties.WORKSHEET_FONT_NAME);
		font.setFontHeightInPoints(SpreadsheetProperties.WORKSHEET_FONT_SIZE);

		return font;
	}

	/**
	 * sets a default font for all cells in the workbook
	 * 
	 * @param workbook
	 * @param color
	 * @param weight
	 * @param fontName
	 * @param fontSize
	 */
	public void setWorkbookDefaultFont(HSSFWorkbook workbook, short color, short weight, String fontName,
			short fontSize) {
		font = workbook.createFont();
		font.setColor(color);
		// font.setBoldweight(weight);
		font.setFontName(fontName);
		font.setFontHeightInPoints(fontSize);
	}

	/**
	 * sets a default font for all cells in the workbook
	 * 
	 * @param workbook
	 * @param fontColor
	 * @param weight
	 * @param fontName
	 * @param fontSize
	 */
	public void setWorkbookDefaultFont(HSSFWorkbook workbook, String fontColor, short weight, String fontName,
			short fontSize) {
		// concatenate a zero if the fontColor is not equal to 6 characters
		while (fontColor.length() < 6) {
			fontColor = fontColor.concat("0");
		}

		ColorConverter colorConverter = new ColorConverter();
		HSSFColor hssfColor = colorConverter.hex2poi(workbook, fontColor);

		setWorkbookDefaultFont(workbook, hssfColor.getIndex(), weight, fontName, fontSize);
	}
}
