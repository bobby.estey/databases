package poi.util;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import poi.exceptions.ClassificationException;

public class ClassificationManager {
	/** font */
	private HSSFFont font = null;

	/** alignment of the font */
	private boolean alignmentSet = false;

	public ClassificationManager() {
	}

	/**
	 * set the classification cell style, the classification value, font and
	 * background attributes and returns the cell style
	 * 
	 * @param workbook
	 * @param classification
	 * @return the classification cell style, the classification value, font and
	 *         background attributes
	 */
	public HSSFCellStyle setCellStyle(HSSFWorkbook workbook, String classification) throws ClassificationException {
		HSSFCellStyle cellStyle = workbook.createCellStyle();

		// cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		// cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		if (font == null) {
			WorkbookAttributes workbookAttributes = new WorkbookAttributes();
			font = workbookAttributes.getWorkbookDefaultFont(workbook);
		}

		// sets the classification background color
		/*
		 * if (classification.equals("FOUO")) { cellStyle.setFillForegroundColor(new
		 * HSSFColor.GREEN().getIndex()); } else if
		 * (classification.equals("UNCLASSIFIED")) {
		 * cellStyle.setFillForegroundColor(new HSSFColor.GREEN().getIndex()); } else if
		 * (classification.equals("CONFIDENTIAL")) {
		 * cellStyle.setFillForegroundColor(new HSSFColor.BLUE().getIndex()); } else if
		 * (classification.equals("SECRET")) { cellStyle.setFillForegroundColor(new
		 * HSSFColor.RED().getIndex()); } else { throw new
		 * ClassificationException("Invalid Classification"); }
		 */
		return cellStyle;
	}

	/**
	 * method writeClassification writes classification header and footers in a
	 * workbook
	 * 
	 * @param workbook
	 * @param classification
	 * @param patriarch
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @throws ClassificationException
	 */
	public void writeClassification(HSSFWorkbook workbook, String classification, HSSFPatriarch patriarch, int x1,
			int y1, int x2, int y2) throws ClassificationException {
		try {
			HSSFTextbox textbox = patriarch
					.createTextbox(new HSSFClientAnchor(0, 0, 0, 0, (short) x1, y1, (short) x2, y2));

			if (font == null) {
				WorkbookAttributes workbookAttributes = new WorkbookAttributes();
				font = workbookAttributes.getWorkbookDefaultFont(workbook);
			}

			if (!alignmentSet) {
				textbox.setHorizontalAlignment(HSSFTextbox.HORIZONTAL_ALIGNMENT_CENTERED);
				textbox.setVerticalAlignment(HSSFTextbox.VERTICAL_ALIGNMENT_CENTER);
			}

			// sets the classification background color
			if (classification.equals("FOUO")) {
				textbox.setFillColor(0, 128, 0);
			} else if (classification.equals("UNCLASSIFIED")) {
				textbox.setFillColor(0, 128, 0);
			} else if (classification.equals("CONFIDENTIAL")) {
				textbox.setFillColor(128, 128, 255);
			} else if (classification.equals("SECRET")) {
				textbox.setFillColor(255, 0, 0);
			} else {
				throw new ClassificationException("Invalid Classification");
			}

			HSSFRichTextString richTextString = new HSSFRichTextString(classification);
			richTextString.applyFont(font);

			textbox.setString(richTextString);
		} catch (Exception exception) {
			exception.printStackTrace();

			throw new ClassificationException(exception.getMessage());
		}
	}
}
