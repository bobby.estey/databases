package poi.util;

public class ColorPalette {
	public final static String[] colorPalette = { "0x000000", // 01 - black
			"0xFFFFFF", // 02 - white
			"0xFF0000", // 03 - red
			"0x00FF00", // 04 - green
			"0x0000FF", // 05 - blue
			"0xFFFF00", // 06 - yellow
			"0xFF00FF", // 07 - magenta
			"0x00FFFF", // 08 - cyan
			"0x800000", // 09
			"0x008000", // 10
			"0x000080", // 11
			"0x808000", // 12
			"0x800080", // 13
			"0x008080", // 14
			"0xC0C0C0", // 15
			"0x808080", // 16
			"0x9999FF", // 17
			"0x993366", // 18
			"0xFFFFCC", // 19
			"0xCCFFFF", // 20
			"0x660066", // 21
			"0xFF8080", // 22
			"0x0066CC", // 23
			"0xCCCCFF", // 24
			// "0x000080", // 25 - same as 11
			// "0xFF00FF", // 26 - same as 7
			// "0xFFFF00", // 27 - same as 6
			// "0x00FFFF", // 28 - same as 8
			// "0x800080", // 29 - same as 13
			// "0x800000", // 30 - same as 9
			// "0x008080", // 31 - same as 14
			// "0x0000FF", // 32 - same as 5
			"0x00CCFF", // 33
			// "0xCCFFFF", // 34 - same as 20
			"0xCCFFCC", // 35
			"0xFFFF99", // 36
			"0x99CCFF", // 37
			"0xFF99CC", // 38
			"0xCC99FF", // 39
			"0xFFCC99", // 40
			"0x3366FF", // 41
			"0x33CCCC", // 42
			"0x99CC00", // 43
			"0xFFCC00", // 44
			"0xFF9900", // 45
			"0xFF6600", // 46
			"0x666699", // 47
			"0x969696", // 48
			"0x003366", // 49
			"0x339966", // 50
			"0x003300", // 51
			"0x333300", // 52
			"0x993300", // 53
			// "0x993366", // 54 - same as 18
			"0x333399", // 55
			"0x333333" // 56
	};
}
