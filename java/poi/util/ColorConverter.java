package poi.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class ColorConverter {
	public int red = 0;
	public int green = 0;
	public int blue = 0;

	/**
	 * ColorConverter Constructor
	 */
	public ColorConverter() {
	}

	/**
	 * 
	 * hex2rgb receives a hex color nnnnnn and converts to red, green, blue
	 * parameters ranging from 0 to 255.
	 * 
	 * @param hexColor - color in hexadecimal
	 */
	public void hex2rgb(String hexColor) {
		if (hexColor.length() != 6)
			return;

		red = Integer.parseInt(hexColor.substring(0, 2), 16);
		green = Integer.parseInt(hexColor.substring(2, 4), 16);
		blue = Integer.parseInt(hexColor.substring(4, 6), 16);
	}

	/**
	 * @return HSSFColor - Apache POI HSSFColor object
	 * 
	 *         WARNING WARNING - This method only replaces colors within the palette
	 *         of the single index AUTOMATIC.
	 */
	@SuppressWarnings("resource")
	public HSSFColor hex2poi(HSSFWorkbook workbook, String hexColor) {
		if (hexColor.length() != 6)
			return null;

		red = Integer.parseInt(hexColor.substring(0, 2), 16);
		green = Integer.parseInt(hexColor.substring(2, 4), 16);
		blue = Integer.parseInt(hexColor.substring(4, 6), 16);

		Color color = new Color(red, green, blue);

		workbook = new HSSFWorkbook();
		HSSFPalette palette = new HSSFWorkbook().getCustomPalette();

		HSSFColor hssfColor = palette.findColor((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());

		if (hssfColor == null) {
			// replace color at index AUTOMATIC
			// palette.setColorAtIndex(HSSFColor.AUTOMATIC.index, (byte) color.getRed(),
			// (byte) color.getGreen(), (byte) color.getBlue());

			// hssfColor =
			// palette.getColor(org.apache.poi.hssf.util.HSSFColor.AUTOMATIC.index);
		}

		return hssfColor;
	}

	/**
	 * updatePalette receives a hex color nnnnnn, spreadsheet palette color index,
	 * e.g. HSSFColor.CORNFLOWER_BLUE.index and converts to an Apache POI HSSFColor
	 * object and updates the color palette.
	 * 
	 * @param workbook - spreadsheet workbook
	 * @param hexColor - color in hexidecimal
	 * @param index    - spreadsheet color index, e.g.
	 *                 HSSFColor.CORNFLOWER_BLUE.index
	 * 
	 * @return HSSFColor - Apache POI HSSFColor object
	 */
	public HSSFColor updatePalette(HSSFWorkbook workbook, String hexColor, short paletteIndex) {
		if (hexColor.length() != 6)
			return null;

		red = Integer.parseInt(hexColor.substring(0, 2), 16);
		green = Integer.parseInt(hexColor.substring(2, 4), 16);
		blue = Integer.parseInt(hexColor.substring(4, 6), 16);

		Color color = new Color(red, green, blue);

		HSSFPalette palette = workbook.getCustomPalette();

		HSSFColor hssfColor = palette.findColor((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());

		if (hssfColor == null) {
			palette.setColorAtIndex(paletteIndex, (byte) color.getRed(), (byte) color.getGreen(),
					(byte) color.getBlue());

			hssfColor = palette.getColor(paletteIndex);
		}

		return hssfColor;
	}

	/**
	 * Apache POI HSSFPalette refers to colors using an offset into the palette
	 * record. The first color in the palette has the index 0x8, the second has the
	 * index 0x9 through 0x40 - 56 decimal
	 * 
	 * getPaletteColors returns an array of the current palette colors
	 * 
	 * @param workbook
	 * 
	 * @return string array
	 */
	public String[] getPaletteColors(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();
		String[] colorArray = new String[56];

		for (short i = 8; i < 64; i++) {
			colorArray[i - 8] = palette.getColor(i).getHexString();
		}

		return colorArray;
	}

	/**
	 * Apache POI HSSFPalette refers to colors using an offset into the palette
	 * record. The first color in the palette has the index 0x8, the second has the
	 * index 0x9 through 0x40 - 56 decimal
	 * 
	 * getPaletteColors returns an array of the current palette colors
	 * 
	 * @param workbook
	 * 
	 * @return color array
	 */
	public List<String> getPaletteColorList(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();

		ArrayList<String> colorArray = new ArrayList<String>();

		for (short i = 8; i < 64; i++) {
			colorArray.add(palette.getColor(i).getHexString());
		}

		return colorArray;
	}
}
