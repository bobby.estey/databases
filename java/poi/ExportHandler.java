package poi;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import poi.config.ExportHandlerProperties;
import poi.exceptions.ClassificationException;
import poi.exceptions.POIException;
import poi.exceptions.SpreadsheetServiceException;
import poi.io.SpreadsheetHandler;
import poi.util.ClassificationManager;
import poi.util.ShapeImage;

/**
 * ExportHandler - exports RMA screens to spreadsheets
 */
public class ExportHandler {
	/** sets up the spreadsheets classification settings */
	private ClassificationManager classificationManager = new ClassificationManager();

	/** sets up shape and image POI utilities */
	private ShapeImage shapeImage = new ShapeImage();

	/** Spreadsheet Workbook */
	private HSSFWorkbook workbook = null;

	/** Spreadsheet Header */
	private SpreadsheetHeader spreadsheetHeader = null;

	/** header array */
	private String[] header;

	/** data array */
	private String[][] data;

	/** worksheet */
	private HSSFSheet sheet = null;

	/** data rows */
	private int dataRows = 0;

	/** data columns */
	private int dataColumns = 0;

	/** output file */
	private String outputFile = "";

	/**
	 * ExportHandler.java
	 * 
	 * @param spreadsheetHeader - the header information
	 * @param header            - the header columns
	 * @param data              - the data to write to the spreadsheet
	 * @throws SpreadsheetServiceException
	 */
	protected ExportHandler(SpreadsheetHeader spreadsheetHeader, String[] header, String[][] data)
			throws SpreadsheetServiceException {
		SpreadsheetHandler spreadsheetHandler = new SpreadsheetHandler();

		this.spreadsheetHeader = spreadsheetHeader;
		this.header = header;
		this.data = data;

		this.dataRows = data.length; // get the number of rows in the array
		this.dataColumns = data[0].length; // get the number of columns in the array

		try {
			// opens up the spreadsheet for output
			spreadsheetHandler.initializeOutputFile();

			this.workbook = spreadsheetHandler.getWorkbook();

			// writes the data
			writeData();

			// saves the spreadsheet
			spreadsheetHandler.writeBook();

			// closes the spreadsheet
			spreadsheetHandler.closeFiles();

			outputFile = spreadsheetHandler.getOutputFile();

		} catch (POIException exception) {
			throw new SpreadsheetServiceException(exception.getMessage());
		} catch (Exception exception) {
			throw new SpreadsheetServiceException(exception.getMessage());
		}
	}

	/**
	 * returns the output file name
	 * 
	 * @return
	 */
	public String getOutputFile() {
		return outputFile;
	}

	/**
	 * method setWorksheet sets up the worksheet within the spreadsheet, e.g.
	 * default fonts, headers, footers, column sizes, headings, etc.
	 */
	private void setWorksheet() {
		HSSFPatriarch patriarch = null;
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		HSSFFont font = workbook.createFont();
		HSSFRow row = null;
		HSSFCell cell = null;

		// initialize workbook and font
		sheet = workbook.createSheet();

		// Clear out any existing shapes on the workbook
		patriarch = sheet.createDrawingPatriarch();

		try {
			// write header classification
			classificationManager.writeClassification(workbook, spreadsheetHeader.getClassification(), patriarch, 0, 0,
					dataColumns, 2);

			// write footer classification
			classificationManager.writeClassification(workbook, spreadsheetHeader.getClassification(), patriarch, 0, 48,
					dataColumns, 50);
		} catch (ClassificationException exception) {
			exception.getMessage();
		}

		// set up column widths to a uniform size
		for (int i = 0; i < dataColumns; i++)
			sheet.setColumnWidth(i, ExportHandlerProperties.MAXIMUM_COLUMN_WIDTH / dataColumns);

		// set the cell style for the workbook
		// font.setColor(new HSSFColor.BLACK().getIndex());
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 16);
		cellStyle.setFont(font);
		// cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		// skip classified header and report heading
		row = sheet.createRow((short) 2);
		cell = row.createCell(0);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(spreadsheetHeader.getReportName());

		// CellRangeAddress cellRangeAddress = new CellRangeAddress(2, 2, 0, dataColumns
		// - 1);
		// sheet.addMergedRegion(cellRangeAddress);

		// write header information, e.g. POC, Last Updated
		String headerInformation = "POC: " + spreadsheetHeader.getPocName() + " " + spreadsheetHeader.getPocNumber()
				+ " Last Updated: " + spreadsheetHeader.getLastUpdated();

		// short fontColor = 0; // new HSSFColor.BLACK().getIndex();
		// shapeImage.setWorkbookDefaultFont(workbook, fontColor,
		// HSSFFont.BOLDWEIGHT_NORMAL, "Arial", (short) 12);

		shapeImage.writeShapeWithoutBackgroundColor(workbook, headerInformation, patriarch, 0, 3, dataColumns, 4);

		// Header Columns
		row = sheet.createRow((short) 5);

		for (int j = 0; j < dataColumns; j++) {
			cell = row.createCell(j);
			cell.setCellValue(header[j]);
		}
	}

	/**
	 * method writeSpreadsheet, reads RMA data and writes to a spreadsheet
	 */
	private void writeData() {
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		HSSFFont font = workbook.createFont();
		HSSFRow row = null;
		HSSFCell cell = null;
		int spreadsheetRow = 1;

		// set the cell style for the workbook
		// font.setColor(new HSSFColor.BLACK().getIndex());
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 10);

		cellStyle.setFont(font);

		// loop through all the data and write to spreadsheet
		for (int i = 0; i < dataRows; i++) {
			// create new workbook when MAXIMUM_ROWS_PER_PAGE have been met
			if (i % ExportHandlerProperties.MAXIMUM_ROWS_PER_PAGE == 0) {
				setWorksheet(); // create new workbook and prepare for data
				spreadsheetRow = 1;
			}

			// skip classified header and create data rows
			row = sheet.createRow((short) spreadsheetRow++ + ExportHandlerProperties.CLASSIFIED_ROW_HEIGHT);

			// write data values in spreadsheet cells
			for (int j = 0; j < dataColumns; j++) {
				cell = row.createCell(j);
				cell.setCellStyle(cellStyle);
				cell.setCellValue(data[i][j]);
			}
		}
	}
}
