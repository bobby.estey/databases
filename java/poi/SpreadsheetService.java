package poi;

import poi.exceptions.SpreadsheetServiceException;
import poi.io.SpreadsheetHandler;

/**
 * SpreadsheetService
 */
public class SpreadsheetService {
	/**
	 * 
	 * SpreadsheetService.java
	 * 
	 */
	public SpreadsheetService() {
	}

	/**
	 * method exportSheet receives objects and exports to a spreadsheet
	 * 
	 * @param spreadsheetHeader the export report header information
	 * @param columnHeaders     the export report column headings
	 * @param data              the export report data
	 * 
	 * @return the output file that was generated
	 * @throws SpreadsheetServiceException
	 */
	public String exportSheet(SpreadsheetHeader spreadsheetHeader, String[] columnHeaders, String[][] data)
			throws SpreadsheetServiceException {
		ExportHandler exportHandler = null;

		try {
			exportHandler = new ExportHandler(spreadsheetHeader, columnHeaders, data);

		} catch (Exception exception) {
			throw new SpreadsheetServiceException(exception.getMessage());
		}

		return exportHandler.getOutputFile();
	}

	/**
	 * main method - has testing code to testing the spreadsheet service
	 * 
	 * @param args
	 * @throws SpreadsheetServiceException
	 */
	public static void main(String[] args) throws SpreadsheetServiceException {
		SpreadsheetService spreadsheetService = new SpreadsheetService();

		// print paths
		SpreadsheetHandler spreadsheetHandler = new SpreadsheetHandler();
		String ssappHome = spreadsheetHandler.getInputDirectory();
		String inputFile = ssappHome + "import.xls";

		System.out.println("SSAPP_HOME: " + ssappHome);
		System.out.println("inputFile:  " + inputFile);

		SpreadsheetHeader spreadsheetHeader = new SpreadsheetHeader("UNCLASSIFIED", "Some Report", "GI JOE",
				"888.888.8888", "10/27/2008 0833Z");

		String[] columnHeaders = { "a0", "b0", "c0", "d0", "e0", "f0", "g0", "h0" };

		String[][] data = { { "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2" }, { "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3" },
				{ "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4" }, { "a5", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "a6", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "a7", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "a8", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "a9", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "aa", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "ab", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ac", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ad", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ae", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "af", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ag", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ah", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ai", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "aj", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ak", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "al", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "am", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "an", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ao", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ap", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "aq", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "ar", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "as", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "at", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "au", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "av", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "aw", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ax", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ay", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "az", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "a0", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "a2", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "a3", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "a4", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "a5", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "a6", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "a7", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "a8", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "a9", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "aa", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "ab", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ac", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ad", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ae", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "af", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ag", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ah", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ai", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "aj", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ak", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "al", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "am", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "an", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "ao", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ap", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "aq", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "ar", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "as", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "at", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "au", "b2", "c2", "d2", "e1", "f1", "g1", "h1" }, { "av", "b3", "c3", "d3", "e1", "f1", "g1", "h1" },
				{ "aw", "b4", "c4", "d4", "e1", "f1", "g1", "h1" }, { "ax", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
				{ "ay", "b2", "c2", "d2", "e1", "f1", "g1", "h1" },
				{ "az", "b3", "c3", "d3", "e1", "f1", "g1", "h1" } };

		// test exportSheeet
		String outputFile = spreadsheetService.exportSheet(spreadsheetHeader, columnHeaders, data);

		System.out.println("Output File:       " + outputFile);

		// test importSheet
		new ImportHandler(0, inputFile);
	}
}
