package poi.exceptions;

/**
 * Exception indicating a failure occured inside the Spreadsheet Service.
 */
public class SpreadsheetServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8101407779397632908L;
	private String msg;

	public SpreadsheetServiceException() {
	}

	/**
	 * @param aMsg
	 * @param t
	 */
	public SpreadsheetServiceException(String aMsg, Throwable t) {
		super(aMsg, t);
		msg = aMsg;
	}

	/**
	 * @param string
	 */
	public SpreadsheetServiceException(String aMsg) {
		super(aMsg);
		msg = aMsg;
	}

	public void setMessage(String aMsg) {
		msg = aMsg;
	}

	public String getMessage() {
		return msg;
	}
}
