package poi.exceptions;

public class POIException extends Exception {

	private static final long serialVersionUID = 1L;
	private String message;

	public POIException() {
	}

	/**
	 * @param message
	 * @param t
	 */
	public POIException(String message, Throwable throwable) {
		super(message, throwable);
		this.message = message;
	}

	/**
	 * @param string
	 */
	public POIException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * @param aMsg
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
