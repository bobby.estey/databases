package threads;

public class ThreadLifecycle extends Thread {

	private static final int NUMBER_THREADS = 10;
	private static final int SLEEP_MILLISECONDS = 5000;

	public void run() {

		for (int i = 0; i < NUMBER_THREADS; i++) {
			System.out.print("ThreadLifecycle.run: ");

			try {
				System.out.println("Thread Sleep");
				ThreadLifecycle.sleep(SLEEP_MILLISECONDS);
				System.out.println("Thread Restart");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {

		ThreadLifecycle threadLifecycle = new ThreadLifecycle();
		System.out.println("State1:   " + threadLifecycle.getState());
		threadLifecycle.start();
		System.out.println("State2:   " + threadLifecycle.getState());
		System.out.println("State3:   " + threadLifecycle.getState());
		System.out.println("State4:   " + threadLifecycle.getState());
		System.out.println("Name:     " + threadLifecycle.getName());
		System.out.println("Priority: " + threadLifecycle.getPriority());
		System.out.println("State5: " + threadLifecycle.getState());

		System.out.println("End Program");
	}
}
