package threads;

public class RunMethodOnly implements Runnable {

	private static final int NUMBER_THREADS = 10;

	@Override
	public void run() {
		System.out.print("RunMethodOnly.run: ");
		for (int i = 0; i < NUMBER_THREADS; i++) {
			System.out.print(i + "rmo ");
		}
	}
}
