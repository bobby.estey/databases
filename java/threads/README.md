# Threads

- What are threads
   - execute multiple programs at the same time (parallel)
       - main program - airplane manufacturer
           - thread 1 (build wings)
           - thread 2 (build fuselage)
           - thread 3 (build engines)
    
- How to create threads, there are two approaches as follows:
   - [extend Thread class](ExtendsThread.java)
   - [implement runnable Interface](ImplementsRunnable.java)
   
- Life cycle of thread (5 phases - New, Runnable, Running, Wait, Terminate) 
    - New - create an instance of a Thread Class or Implement runnable Interface, creating an object of Class Thread
    - Runnable - thread ready to start, the start() method is called but the thread hasn't been scheduled Thread Scheduler (TS)
    - Running - the TS, selects the thread to execute and the thread is running
    - Wait (Non-Runnable / Block) - thread sleeping, still available, however, not eligible to execute and waiting for the TS to schedule to execute
    - Terminated (Dead) - thread destroyed, the object is close, null and terminated
    
    Note:  Thread Scheduler (TS) - the manager of all threads executing.
    
- Thread methods
   - start() - start the current thread execution
   - sleep() - make the the current thread sleep (pause)
   - setPriority() - set the priority of the thread
   - join() - wait for another thread to die
   - isAlive() - test the thread, e.g. heartbeat
   
# Things that can go wrong with Threads
    - synchronization - threads can access the same data at the same time.  For Instance:
        -  Thread A reads from Memory Address 100 and receives the value 10, has a method that doubles the value and writes the new value
        -  Thread B reads from Memory Address 100 and receives the value 10 (not 20) because Thread A did not complete the operation in time
        -  NOTE:  Java has the synchronized keyword that locks out methods so only one object can use the method at a time
    - locks can cause performance issues - synchronized locks the methods so other threads have to wait before processing
    - thread exceptions must be handled by the correct thread or the entire application can shut down
        -  Thread A is executing fine
        -  Thread B has an exception, however, the Threads are not managed correctly and Thread A is accidently shut down with the possibility of other Threads shutting down
        
        