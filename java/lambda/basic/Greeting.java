package lambda.basic;

// see recording 12 what this annotation does
// Lambda only works with one method, if there are more than one method defined in the Interface then @FunctionalInterface
// will cause a compiler error notifying the develop that this Interface was designed as a Lambda Expression
@FunctionalInterface
public interface Greeting {

	public void perform();
	// public void anotherMethod();  // will cause a compiler error
}
