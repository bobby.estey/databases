package lambda.functional.interfaces;

import java.util.function.IntUnaryOperator;

public class Factorial {

	// x == 0 ? 1 -> if x equals 0 then x = 1 else compute the factorial
	public static IntUnaryOperator factorial = x -> x == 0 ? 1 : x * Factorial.factorial.applyAsInt(x - 1);

	public static void main(String[] args) {
		System.out.println(factorial.applyAsInt(5));
	}
}
