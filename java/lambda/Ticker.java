package lambda;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class Ticker {

	public static void main(String[] args) {

		Timer timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				System.out.println("Ticker");
			}
		});

		Timer timer2 = new Timer(1000, e -> {
			System.out.println("Ticker");
		});

		timer2.addActionListener(listener -> {
			System.out.println("Ticker");
		});

		timer.start();
	}
}
