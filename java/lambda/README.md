# Learning Lambda

##Links

### Unit 1
- [Lambda Basics 1](https://www.youtube.com/watch?v=gpIUfj3KaOc)
    - Understanding Lambdas
    - Using Lambdas
    - Functional Interfaces
    - Method References
    - Collections Improvements
- [Lambda Basics 2](https://www.youtube.com/watch?v=BiknDHHPwjk)
    - Why Lambdas - Enables functional programming - transition from object oriented to functional programming
    - Readable and concise code 
        - eliminate boiler plate code (Classes) traditionally written before
        - write a quick function
    - Easier to use APIs and libraries
    - Support for parallel processing
- [Lambda Basics 3](https://www.youtube.com/watch?v=4mmOTCIZI1M)
    - eclipse setup
- [Lambda Basics 4](https://www.youtube.com/watch?v=J5RICKnbuKI)
    - functional programming - writing easier to read, elegant and maintainable code and not a replacement for object oriented programming
- [Lambda Basics 5](https://www.youtube.com/watch?v=nL7H4F_ly_k)
    - object oriented - things and nouns - passing value, everything is an object and code is associated with classes
    - functional - actions and verbs - passing behavior, isolated code
- [Lambda Basics 6](https://www.youtube.com/watch?v=nUIAvs4OEkM)
    - NOTE:  Greeter.java - greeter.greet - passing in a Class that contains a behavior to the greet method
    - AHA!!! MOMENT - Lambdas pass in an action which are just functions that are called "Lambda Expressions"
    - Lambda Expressions do not belong to a Class, live in isolation and can be VALUES, Functions as Values
    - function = public String perform() { ... } converts to function = () -> { ... }    // -> is also called the FAT ARROW
    - if only one line of code, "{" and "}" can be removed:  function = () -> return "Lambda";
    - ANOTHER NOTE:  return is optional, e.g.  function = () -> "Lambda";  // return optional
- [Lambda Basics 7](https://www.youtube.com/watch?v=BK5iSG5yMT0)
    - passing a Lambda example inside the method signature:  greet(() -> "Lambda"));  // return optional
    - passing parameters 
        - returning a double function:  doubleFunction = (int i) -> i * 3.14;  // return optional
        - returning a addition function:  addFunction = (int a, int b) -> a + b;
        - division function:  divideFunction = (int a, int b) -> { if (b == 0) return 0; return a / b; };
        - string function: stringLengthCountFunction = (String string) -> s.length();
- [Lambda Basics 8](https://www.youtube.com/watch?v=DNE8OYBzj7E)
     - adding interfaces
- [Lambda Basics 9](https://www.youtube.com/watch?v=kpK2e343v48)
     - calling a Lambda Expression
     - Lambda Expressions are somewhat similar to  a short cut inner / anonymous inner Class
- [Lambda Basics 10](https://www.youtube.com/watch?v=a8jvxBbswp4)
     - type inference - the compile figures out the expression type
     - StringLengthLambda stringLengthLambda = string -> string.length();
- [Lambda Basics 11](https://www.youtube.com/watch?v=9u8CWKuMCvM)
     - Runnable Using Lambdas
- [Lambda Basics 12](https://www.youtube.com/watch?v=mJgwVdEGg5A)
     - Functional Interface - @FunctionalInterface - a function with only one abstract method
- [Lambda Basics 13](https://www.youtube.com/watch?v=YC82QKigdsY)
     - Lambda Exercises
- [Lambda Basics 14](https://www.youtube.com/watch?v=MqsCdbMQjWc)
     - [Java 7 Solution 00:00](https://www.youtube.com/watch?v=MqsCdbMQjWc)
     - [Java 8 Solution 11:10](https://www.youtube.com/watch?v=MqsCdbMQjWc)
     
### Unit 2
- [Lambda Basics 15](https://www.youtube.com/watch?v=yubVRLP9Htw)
    